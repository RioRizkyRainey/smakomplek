<?php
/*
 * DEPENDENCIES:
 * woocommerce/
 * wc-vendors/
 * vc_templates/azexo_deal_search_form.php
 * page-templates/with-shop-sidebar.php
 * js/wc_deals.js
 */
global $azexo_templates;
if (!isset($azexo_templates)) {
    $azexo_templates = array();
}


global $azwoo_templates;

$azwoo_templates = array(
    'single_product' => __('Single product', 'AZEXO'),
    'shop_product' => __('Shop product', 'AZEXO'),
    'featured_product' => __('Featured product', 'AZEXO'),
    'related_product' => __('Related product', 'AZEXO'),
    'upsells_product' => __('Up-sells product', 'AZEXO'),
    'best_rated_product' => __('Best rated product', 'AZEXO'),
    'latest_product' => __('Latest product', 'AZEXO'),
);

$azexo_templates = array_merge($azexo_templates, $azwoo_templates);


global $azexo_fields;
if (!isset($azexo_fields)) {
    $azexo_fields = array();
}

global $azwoo_fields;

$azwoo_fields = array(
    'purchased' => __('Purchased', 'AZEXO'),
    'discount' => __('Discount', 'AZEXO'),
    'deal_time_left' => __('Deal time left', 'AZEXO'),
    'price_offer' => __('Price offer', 'AZEXO'),
    'price_deal' => __('Price deal', 'AZEXO'),
    'price' => __('Price', 'AZEXO'),
    'price_trimmed' => __('Price (trimmed zeros)', 'AZEXO'),
    'product_rating' => __('Average product rating', 'AZEXO'),
    'add_to_cart' => __('Add to cart link', 'AZEXO'),
    'loop_sale_flash' => __('Loop product sale flash', 'AZEXO'),
    'loop_rating' => __('Loop product average rating', 'AZEXO'),
    'loop_price' => __('Loop product price', 'AZEXO'),
    'loop_add_to_cart' => __('Loop product add to cart link', 'AZEXO'),
    'single_sale_flash' => __('Single product sale flash', 'AZEXO'),
    'single_add_to_cart' => __('Single product add to cart', 'AZEXO'),
    'single_rating' => __('Single product average rating', 'AZEXO'),
    'single_price' => __('Single product price', 'AZEXO'),
    'single_meta' => __('Single product meta', 'AZEXO'),
    'single_sharing' => __('Single product sharing', 'AZEXO'),
    'single_title' => __('Single product title', 'AZEXO'),
    'single_summary' => __('Single product summary', 'AZEXO'),
    'purchased_deals' => __('Single purchased deals', 'AZEXO'),
    'seller_info' => __('Single seller info', 'AZEXO'),
    'sold_by' => __('Sold by info', 'AZEXO'),
);

$azexo_fields = array_merge($azexo_fields, $azwoo_fields);

global $azexo_fields_post_types;
if (!isset($azexo_fields_post_types)) {
    $azexo_fields_post_types = array();
}
$azexo_fields_post_types = array_merge($azexo_fields_post_types, array_combine(array_keys($azwoo_fields), array_fill(0, count(array_keys($azwoo_fields)), 'product')));


add_action('after_setup_theme', 'azwoo_after_setup_theme');

function azwoo_after_setup_theme() {
    add_theme_support('woocommerce');
}

function azwoo_tgmpa_register() {

    $plugins = array(
        array(
            'name' => 'WooCommerce',
            'slug' => 'woocommerce',
            'required' => true,
        ),
        array(
            'name' => 'WC Vendors',
            'slug' => 'wc-vendors',
            'required' => true,
        ),
        array(
            'name' => 'WooCommerce Deals',
            'slug' => 'wc_deals',
            'source' => get_template_directory() . '/plugins/wc_deals.zip',
            'required' => true,
        ),
    );
    tgmpa($plugins, array());
}

add_action('tgmpa_register', 'azwoo_tgmpa_register');

add_filter('woocommerce_enqueue_styles', '__return_false');
add_action('widgets_init', 'azwoo_widgets_init');

function azwoo_widgets_init() {
    if (function_exists('register_sidebar')) {
        register_sidebar(array('name' => 'Shop sidebar', 'id' => "shop", 'before_widget' => '<div id="%1$s" class="widget %2$s">', 'after_widget' => '</div>', 'before_title' => '<div class="widget-title"><h3>', 'after_title' => '</h3></div>'));
    }
    register_widget('AZWOORelatedProducts');
    register_widget('AZWOOUpsellProducts');
}

class AZWOORelatedProducts extends WP_Widget {

    function AZWOORelatedProducts() {
        parent::__construct('azwoo_related_products', AZEXO_THEME_NAME . ' - WooCommerce related products');
    }

    function widget($args, $instance) {
        if (is_product()) {
            woocommerce_output_related_products();
        }
    }

}

class AZWOOUpsellProducts extends WP_Widget {

    function AZWOOUpsellProducts() {
        parent::__construct('azwoo_upsell_products', AZEXO_THEME_NAME . ' - WooCommerce upsell products');
    }

    function widget($args, $instance) {
        if (is_product()) {
            woocommerce_upsell_display();
        }
    }

}

add_action('init', 'azwoo_init');

function azwoo_init() {
    wp_register_script('azwoo', get_template_directory_uri() . '/js/azwoo.js', array('jquery'), AZEXO_THEME_VERSION, true);
    wp_enqueue_script('azwoo');
    wp_register_script('azwoo_deals', get_template_directory_uri() . '/js/azwoo_deals.js', array('jquery'), AZEXO_THEME_VERSION, true);
    wp_enqueue_script('azwoo_deals');
    wp_register_script('countdown', get_template_directory_uri() . '/js/jquery.countdown.min.js', array('jquery'), AZEXO_THEME_VERSION, true);

    if (class_exists('WooCommerce')) {
        $lightbox = get_option('woocommerce_enable_lightbox');
        if($lightbox == 'yes') {
            update_option('woocommerce_enable_lightbox', '');
        }
        
        remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);
        remove_action('woocommerce_after_shop_loop', 'woocommerce_pagination', 10);
        add_action('woocommerce_after_main_content', 'woocommerce_pagination');
        add_action('woocommerce_share', 'azexo_entry_share');


        remove_action('woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
        remove_action('woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);

        $options = get_option(AZEXO_THEME_NAME);
        if (isset($options['show_breadcrumbs']) && !$options['show_breadcrumbs']) {
            remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);
        }

        remove_action('woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10);
        remove_action('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10);

        remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5);
        remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10);

        remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);

        remove_action('woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10);
        remove_action('woocommerce_before_single_product_summary', 'woocommerce_show_product_images', 20);

        remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_title', 5);
        remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10);
        remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);
        remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20);
        remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30);
        remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
        remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50);



        if (class_exists('Jetpack_Widget_Conditions')) {
            remove_filter('sidebars_widgets', array('Jetpack_Widget_Conditions', 'sidebars_widgets')); //FIX is_active_widget wrong result
        }
        if (is_active_widget(false, false, 'azwoo_related_products')) {
            remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);
        }
        if (is_active_widget(false, false, 'azwoo_upsell_products')) {
            remove_action('woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15);
        }
    }

    if (class_exists('WCV_Vendor_Shop')) {
        remove_filter('woocommerce_after_shop_loop_item', array('WCV_Vendor_Shop', 'template_loop_sold_by'), 9);

        remove_filter('woocommerce_product_tabs', array('WCV_Vendor_Shop', 'seller_info_tab'));

        remove_filter('post_type_archive_link', array('WCV_Vendor_Shop', 'change_archive_link')); //function make infinite recursion
        add_filter('admin_init', array('WCV_Vendor_Shop', 'change_archive_link')); //FIX from https://wordpress.org/support/topic/nesting-level
    }
}

add_action('wp_enqueue_scripts', 'azwoo_styles');

function azwoo_styles() {
    //move styles to header for HTML5 validation
    if (class_exists('WooCommerce')) {
        wp_enqueue_style('select2', str_replace(array('http:', 'https:'), '', WC()->plugin_url()) . '/assets/' . 'css/select2.css');
    }
    wp_enqueue_style('vc_linecons');
}

function azwoo_deal_time_left() {
    $deal_expire = get_post_meta(get_the_ID(), '_sale_price_dates_to', true);
    if (!empty($deal_expire)) {
        $expire = $deal_expire - current_time('timestamp');
        if ($expire < 0)
            $expire = 0;
        $days = floor($expire / 60 / 60 / 24);
        $hours = floor(($expire - $days * 60 * 60 * 24) / 60 / 60);
        $minutes = floor(($expire - $days * 60 * 60 * 24 - $hours * 60 * 60) / 60);
        $seconds = $expire - $days * 60 * 60 * 24 - $hours * 60 * 60 - $minutes * 60;
        wp_enqueue_script('countdown');
        ?>
        <div class="time-left">
            <div class="call-to-action"><?php _e('Hurry up! Time left:', 'AZEXO'); ?></div>
            <div class="time" data-time="<?php print date('Y/m/d H:i:s', $deal_expire); ?>">
                <div class="days"><span class="count"><?php print $days; ?></span><span class="title"><?php print __('d.', 'AZEXO'); ?></span></div>
                <div class="hours"><span class="count"><?php print $hours; ?></span><span class="title"><?php print __('h.', 'AZEXO'); ?></span></div>
                <div class="minutes"><span class="count"><?php print $minutes; ?></span><span class="title"><?php print __('m.', 'AZEXO'); ?></span></div>
                <div class="seconds"><span class="count"><?php print $seconds; ?></span><span class="title"><?php print __('s.', 'AZEXO'); ?></span></div>
            </div>
        </div>
        <?php
    }
}

function azwoo_price_offer() {
    global $product;
    ?>
    <div class="price-offer">
        <?php
        if ($product->product_type == 'variable') {
            $display_price = wc_price($product->get_display_price($product->get_variation_price())) . $product->get_price_suffix();
            $display_regular_price = wc_price($product->get_display_price($product->get_variation_regular_price())) . $product->get_price_suffix();
            $discount = round((1 - $product->get_display_price($product->get_variation_price()) / $product->get_display_price($product->get_variation_regular_price())) * 100) . '%';
            $savings = wc_price($product->get_display_price($product->get_variation_regular_price()) - $product->get_display_price($product->get_variation_price()));
        } else {
            $display_price = wc_price($product->get_display_price()) . $product->get_price_suffix();
            $display_regular_price = wc_price($product->get_display_price($product->get_regular_price())) . $product->get_price_suffix();
            $discount = round(($product->get_display_price() / $product->get_display_price($product->get_regular_price()) - 1) * 100) . '%';
            $savings = wc_price($product->get_display_price($product->get_regular_price()) - $product->get_display_price());
        }
        ?>
        <div class="discount">
            <?php print $discount; ?>
        </div>
        <div class="regular-price">
            <?php echo apply_filters('woocommerce_get_price_html', apply_filters('woocommerce_price_html', $display_regular_price, $product), $product); ?>
        </div>
        <div class="price">
            <?php print $display_price; ?>
        </div>
    </div>
    <?php
}

function azwoo_price_deal() {
    global $product;
    if ($product->product_type == 'variable') {
        $display_regular_price = wc_price($product->get_display_price($product->get_variation_regular_price())) . $product->get_price_suffix();
        $discount = round((1 - $product->get_display_price($product->get_variation_price()) / $product->get_display_price($product->get_variation_regular_price())) * 100) . '%';
        $savings = wc_price($product->get_display_price($product->get_variation_regular_price()) - $product->get_display_price($product->get_variation_price()));
    } else {
        $display_regular_price = wc_price($product->get_display_price($product->get_regular_price())) . $product->get_price_suffix();
        $discount = round((1 - $product->get_display_price() / $product->get_display_price($product->get_regular_price())) * 100) . '%';
        $savings = wc_price($product->get_display_price($product->get_regular_price()) - $product->get_display_price());
    }
    ?>
    <div class="price-deal">
        <div class="regular-price">
            <?php echo apply_filters('woocommerce_get_price_html', apply_filters('woocommerce_price_html', $display_regular_price, $product), $product); ?>
            <span><?php _e('Value', 'AZEXO'); ?></span>
        </div>
        <div class="discount">
            <span class="amount"><?php print $discount; ?></span>
            <span><?php _e('Discount', 'AZEXO'); ?></span>
        </div>
        <div class="savings">
            <?php print $savings; ?>
            <span><?php _e('Savings', 'AZEXO'); ?></span>
        </div>
    </div>
    <?php
}

add_filter('azexo_entry_field', 'azwoo_entry_field', 10, 2);

function azwoo_entry_field($output, $name) {
    global $product;
    switch ($name) {
        case 'purchased':
            return '<span class="purchased">' . wcd_get_deal_purchases(get_the_ID()) . '</span>';
            break;
        case 'discount':
            if ($product->product_type == 'variable') {
                $discount = round(($product->get_display_price($product->get_variation_price()) / $product->get_display_price($product->get_variation_regular_price()) - 1) * 100) . '%';
            } else {
                $discount = round(($product->get_display_price() / $product->get_display_price($product->get_regular_price()) - 1) * 100) . '%';
            }
            return '<span class="discount">' . $discount . '</span>';
            break;
        case 'deal_time_left':
            ob_start();
            azwoo_deal_time_left();
            return ob_get_clean();
            break;
        case 'price_offer':
            ob_start();
            azwoo_price_offer();
            $price_offer = ob_get_clean();
            return $price_offer;
            break;
        case 'price_deal':
            ob_start();
            add_filter('woocommerce_price_trim_zeros', '__return_true');
            azwoo_price_deal();
            remove_filter('woocommerce_price_trim_zeros', '__return_true');
            $price_deal = ob_get_clean();
            return $price_deal;
            break;
        case 'price':
            return '<span class="price">' . $product->get_price_html() . '</span>';
            break;
        case 'price_trimmed':
            add_filter('woocommerce_price_trim_zeros', '__return_true');
            $price = '<span class="price">' . $product->get_price_html() . '</span>';
            remove_filter('woocommerce_price_trim_zeros', '__return_true');
            return $price;
            break;
        case 'product_rating':
            return $product->get_rating_html();
            break;
        case 'add_to_cart':
            ob_start();
            woocommerce_template_loop_add_to_cart();
            return '<span class="add-to-cart">' . ob_get_clean() . '</span>';
            break;
        case 'loop_sale_flash':
            ob_start();
            woocommerce_show_product_loop_sale_flash();
            return ob_get_clean();
            break;
        case 'loop_rating':
            ob_start();
            woocommerce_template_loop_rating();
            return ob_get_clean();
            break;
        case 'loop_price':
            ob_start();
            woocommerce_template_loop_price();
            return ob_get_clean();
            break;
        case 'loop_add_to_cart':
            ob_start();
            woocommerce_template_loop_add_to_cart();
            return ob_get_clean();
            break;
        case 'single_sale_flash':
            ob_start();
            woocommerce_show_product_sale_flash();
            return ob_get_clean();
            break;
        case 'single_rating':
            ob_start();
            woocommerce_template_single_rating();
            return ob_get_clean();
            break;
        case 'single_price':
            ob_start();
            woocommerce_template_single_price();
            return ob_get_clean();
            break;
        case 'single_add_to_cart':
            ob_start();
            woocommerce_template_single_add_to_cart();
            return ob_get_clean();
            break;
        case 'single_meta':
            ob_start();
            woocommerce_template_single_meta();
            return ob_get_clean();
            break;
        case 'single_sharing':
            ob_start();
            woocommerce_template_single_sharing();
            return ob_get_clean();
            break;
        case 'single_title':
            ob_start();
            woocommerce_template_single_title();
            return ob_get_clean();
            break;
        case 'single_summary':
            ob_start();
            woocommerce_template_single_excerpt();
            return ob_get_clean();
            break;
        case 'purchased_deals':
            $output = '<div class="purchased-deals-wrapper">';
            $output .= '<span class="deals">' . wcd_get_deal_purchases(get_the_ID()) . ' ' . __('deals', 'AZEXO') . '</span>' . "\n";
            $output .= '<span class="purchased">' . __('Purchased', 'AZEXO') . '</span>';
            $output .= '</div>';
            return $output;
            break;
        case 'seller_info':
            $output = '<div class="title"><h3>' . __('Seller info', 'AZEXO') . '</h3></div>';
            if (class_exists('WC_Vendors')) {
                global $post, $product, $woocommerce;
                $seller_info = get_user_meta($post->post_author, 'pv_seller_info', true);
                $has_html = get_user_meta($post->post_author, 'pv_shop_html_enabled', true);
                $global_html = WC_Vendors::$pv_options->get_option('shop_html_enabled');
                if (!empty($seller_info)) {
                    $seller_info = do_shortcode($seller_info);
                    $output .= '<div class="pv_seller_info">';
                    $output .= apply_filters('wcv_before_seller_info_tab', '');
                    $output .= ( $global_html || $has_html ) ? wpautop(wptexturize(wp_kses_post($seller_info))) : sanitize_text_field($seller_info);
                    $output .= apply_filters('wcv_after_seller_info_tab', '');
                    $output .= '</div>';
                }
            }
            return $output;
            break;
        case 'sold_by':
            if (class_exists('WCV_Vendor_Shop')) {
                ob_start();
                WCV_Vendor_Shop::template_loop_sold_by();
                return ob_get_clean();
            }
            break;
    }
    return $output;
}

function azwoo_get_images_links($thumbnail_size) {
    global $product;
    $images_links = array();
    if (has_post_thumbnail()) {
        $image_link = azexo_get_attachment_thumbnail(get_post_thumbnail_id(), $thumbnail_size, true);
        $images_links[] = $image_link[0];
    }
    $attachment_ids = $product->get_gallery_attachment_ids();
    if ($attachment_ids) {
        foreach ($attachment_ids as $attachment_id) {
            $image_link = azexo_get_attachment_thumbnail($attachment_id, $thumbnail_size, true);
            if (!empty($image_link))
                $images_links[] = $image_link[0];
        }
    }
    $images_links = array_unique($images_links);
    return $images_links;
}

add_filter('woocommerce_price_format', 'azwoo_price_format', 10, 2);

function azwoo_price_format($format, $currency_pos) {
    $format = '%1$s%2$s';

    switch ($currency_pos) {
        case 'left' :
            $format = '<span class="currency">%1$s</span>%2$s';
            break;
        case 'right' :
            $format = '%2$s<span class="currency">%1$s</span>';
            break;
        case 'left_space' :
            $format = '<span class="currency">%1$s</span>&nbsp;%2$s';
            break;
        case 'right_space' :
            $format = '%2$s&nbsp;<span class="currency">%1$s</span>';
            break;
    }
    return $format;
}

add_filter('woocommerce_product_categories_widget_args', 'azwoo_product_categories_widget_args');

function azwoo_product_categories_widget_args($list_args) {

    if (class_exists('WC_Product_Cat_List_Walker')) {

        if (!class_exists('AZEXO_Product_Cat_List_Walker')) {

            class AZEXO_Product_Cat_List_Walker extends WC_Product_Cat_List_Walker {

                public function start_el(&$output, $cat, $depth = 0, $args = array(), $current_object_id = 0) {
                    $output .= '<li class="cat-item cat-item-' . $cat->term_id;

                    if ($args['current_category'] == $cat->term_id) {
                        $output .= ' current-cat';
                    }

                    if ($args['has_children'] && $args['hierarchical']) {
                        $output .= ' cat-parent';
                    }

                    if ($args['current_category_ancestors'] && $args['current_category'] && in_array($cat->term_id, $args['current_category_ancestors'])) {
                        $output .= ' current-cat-parent';
                    }

                    $thumbnail_id = get_woocommerce_term_meta($cat->term_id, 'thumbnail_id', true);
                    $image = wp_get_attachment_thumb_url($thumbnail_id);
                    if ($image) {
                        $image = '<img alt="" src="' . esc_url($image) . '">';
                    }

                    $output .= '"><a href="' . esc_url(get_term_link((int) $cat->term_id, 'product_cat')) . '">' . $image . '<span>' . $cat->name . '</span></a>';

                    if ($args['show_count']) {
                        $output .= ' <span class="count">(' . $cat->count . ')</span>';
                    }
                }

            }

        }
        $list_args['walker'] = new AZEXO_Product_Cat_List_Walker;
    }
    return $list_args;
}

add_filter('azexo_posts_list_template_path', 'azwoo_posts_list_template_path', 10, 2);

function azwoo_posts_list_template_path($template, $template_name) {
    global $azwoo_templates;
    if (in_array($template_name, array_keys($azwoo_templates))) {
        return array("content-product.php", WC()->template_path() . "content-product.php");
    } else {
        return $template;
    }
}

function azwoo_order_by_rating_post_clauses($args) {
    global $wpdb;

    $args['fields'] .= ", AVG( $wpdb->commentmeta.meta_value ) as average_rating ";

    $args['where'] .= " AND ( $wpdb->commentmeta.meta_key = 'rating' OR $wpdb->commentmeta.meta_key IS null ) ";

    $args['join'] .= "
			LEFT OUTER JOIN $wpdb->comments ON($wpdb->posts.ID = $wpdb->comments.comment_post_ID)
			LEFT JOIN $wpdb->commentmeta ON($wpdb->comments.comment_ID = $wpdb->commentmeta.comment_id)
		";

    $args['orderby'] = "average_rating DESC, $wpdb->posts.post_date DESC";

    $args['groupby'] = "$wpdb->posts.ID";

    return $args;
}

function azwoo_featured_post_clauses($args) {
    global $wpdb;

    $args['join'] .= " INNER JOIN {$wpdb->postmeta} as mf ON $wpdb->posts.ID = mf.post_id ";

    $args['where'] .= " AND ( mf.meta_key = '_featured') AND ( mf.meta_value = 'yes' ) ";

    return $args;
}

add_filter('woocommerce_short_description', 'azwoo_short_description');

function azwoo_short_description($description) {
    $options = get_option(AZEXO_THEME_NAME);
    $excerpt = wp_trim_words(wp_strip_all_tags($description), isset($options['excerpt_length']) ? $options['excerpt_length'] : 15);
    return $excerpt;
}

add_filter('wp_nav_menu_objects', 'azwoo_wp_nav_menu_objects', 10, 2);

function azwoo_wp_nav_menu_objects($sorted_menu_items, $args) {
    $woocommerce_myaccount_page_id = get_option('woocommerce_myaccount_page_id');
    if (!is_user_logged_in()) {
        foreach ($sorted_menu_items as $i => &$menu_item) {
            if ($menu_item->object_id == $woocommerce_myaccount_page_id && $menu_item->object == 'page') {
                $menu_item->title = __('Login/Register', 'AZEXO');
            }
        }
    }
    $remove_array = array();
    if (class_exists('WC_Vendors')) {
        $remove_array[] = WC_Vendors::$pv_options->get_option('vendor_dashboard_page');
        $remove_array[] = WC_Vendors::$pv_options->get_option('shop_settings_page');
        $remove_array[] = WC_Vendors::$pv_options->get_option('wcv_orders');
    }
    if (class_exists('WCV_Vendors')) {
        if (!WCV_Vendors::is_vendor(get_current_user_id())) {
            foreach ($sorted_menu_items as $i => &$menu_item) {
                if (in_array($menu_item->object_id, $remove_array) && $menu_item->object == 'page') {
                    unset($sorted_menu_items[$i]);
                    prev($sorted_menu_items);
                }
            }
        }
    }
    return $sorted_menu_items;
}

add_filter('pre_get_posts', 'wcd_pre_get_posts');

function wcd_pre_get_posts($query) {
    if (!is_admin()) {
        $post_type = $query->get('post_type');
        if (!is_array($post_type)) {
            $post_type = array($post_type);
        }
        if ((in_array('product', $post_type) && count($post_type) == 1) || $query->get('product_cat') || $query->get('product_tag') || $query->get('location')) {
            $meta_query = $query->get('meta_query');
            $meta_query[] = array(
                'key' => '_sale_price_dates_from',
                'value' => current_time('timestamp'),
                'compare' => '<'
            );
            $meta_query[] = array(
                'key' => '_sale_price_dates_to',
                'value' => current_time('timestamp'),
                'compare' => '>'
            );
            $query->set('meta_query', $meta_query);
        }
    }
    return $query;
}

if (class_exists('WPBakeryShortCode') && function_exists('vc_map')) {

    class WPBakeryShortCode_azexo_deal_search_form extends WPBakeryShortCode {
        
    }

    vc_map(array(
        "name" => "AZEXO - Deal Search Form",
        "base" => "azexo_deal_search_form",
        'category' => __('AZEXO', 'AZEXO'),
        'show_settings_on_create' => false,
    ));

    class WPBakeryShortCode_azexo_product_fields_wrapper extends WPBakeryShortCodesContainer {
        
    }

    vc_map(array(
        "name" => "AZEXO - Product Fields Wrapper",
        "base" => "azexo_product_fields_wrapper",
        'category' => __('AZEXO', 'AZEXO'),
        "as_parent" => array('except' => 'azexo_product_fields_wrapper'),
        "content_element" => true,
        "controls" => "full",
        "show_settings_on_create" => true,
        "is_container" => true,
        'params' => array(
            array(
                'type' => 'textfield',
                'heading' => __('Extra class name', 'AZEXO'),
                'param_name' => 'el_class',
                'description' => __('If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'AZEXO'),
            ),
            array(
                'type' => 'css_editor',
                'heading' => __('Css', 'AZEXO'),
                'param_name' => 'css',
                'group' => __('Design options', 'AZEXO'),
            ),
        ),
        "js_view" => 'VcColumnView'
    ));
}

if (class_exists('WPLessPlugin')) {
    $less = WPLessPlugin::getInstance();
    $options = get_option(AZEXO_THEME_NAME);
    if (isset($options['accent-1-color']))
        $less->addVariable('accent-1-color', $options['accent-1-color']);
    if (isset($options['accent-2-color']))
        $less->addVariable('accent-2-color', $options['accent-2-color']);
}