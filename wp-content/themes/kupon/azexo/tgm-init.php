<?php

function azexo_tgmpa_register() {

    $plugins = array(
        array(
            'name' => 'WP-LESS',
            'slug' => 'wp-less',
            'required' => true,
        ),
        array(
            'name' => 'WordPress Importer',
            'slug' => 'wordpress-importer',
            'required' => true,
        ),
        array(
            'name' => 'WPBakery Visual Composer',
            'slug' => 'js_composer',
            'source' => get_template_directory() . '/plugins/js_composer.zip',
            'required' => true,
        ),
        array(
            'name' => 'Visual Composer Widgets',
            'slug' => 'vc_widgets',
            'source' => get_template_directory() . '/plugins/vc_widgets.zip',
            'required' => true,
        ),
        array(
            'name' => 'JP Widget Visibility',
            'slug' => 'jetpack-widget-visibility',
        ),
        array(
            'name' => 'Contact Form 7',
            'slug' => 'contact-form-7',
        ),
        array(
            'name' => 'Custom Sidebars',
            'slug' => 'custom-sidebars',
        ),        
        array(
            'name' => 'Widget - Flickr Badge Widget',
            'slug' => 'flickr-badges-widget',
        ),
        array(
            'name' => 'WP Instagram Widget',
            'slug' => 'wp-instagram-widget',
        ),
    );
    tgmpa($plugins, array());

    $additional_plugins = array('az_sport_club' => 'AZEXO Sport Club','az_locations' => 'AZEXO Locations', 'circular_countdown' => 'Circular CountDown');
    foreach ($additional_plugins as $additional_plugin_slug => $additional_plugin_name) {
        $plugin_path = get_template_directory() . '/plugins/' . $additional_plugin_slug . '.zip';
        if (file_exists($plugin_path)) {
            $plugin = array(
                array(
                    'name' => $additional_plugin_name,
                    'slug' => $additional_plugin_slug,
                    'source' => $plugin_path,
                    'required' => true,
                ),
            );
            tgmpa($plugin, array());
        }
    }
}

add_action('tgmpa_register', 'azexo_tgmpa_register');
