<?php

if (function_exists('vc_remove_param')) {
    vc_remove_param('vc_row', 'full_width');
}

if (class_exists('WPBakeryShortCode') && function_exists('vc_map')) {

    class WPBakeryShortCode_azexo_search_form extends WPBakeryShortCode {
        
    }

    vc_map(array(
        "name" => "AZEXO - Search Form",
        "base" => "azexo_search_form",
        'category' => __('AZEXO', 'AZEXO'),
        "content_element" => true,
        "controls" => "full",
        "show_settings_on_create" => false
    ));

    class WPBakeryShortCode_azexo_post extends WPBakeryShortCode {
        
    }

    global $azexo_templates;
    if (!isset($azexo_templates)) {
        $azexo_templates = array();
    }
    vc_map(array(
        "name" => "AZEXO - Post",
        "base" => "azexo_post",
        'category' => __('AZEXO', 'AZEXO'),
        "content_element" => true,
        "controls" => "full",
        "show_settings_on_create" => true,
        'params' => array(
            array(
                'type' => 'textfield',
                'heading' => __('Post ID', 'AZEXO'),
                'param_name' => 'post_id',
                'description' => __('Post ID', 'AZEXO'),
            ),
            array(
                'type' => 'dropdown',
                'heading' => __('Post template', 'AZEXO'),
                'param_name' => 'template',
                'value' => array_merge(array(__('Default', 'AZEXO') => 'post'), array_flip($azexo_templates)),
                'description' => __('Post template.', 'AZEXO'),
                'admin_label' => true
            ),
            array(
                'type' => 'textfield',
                'heading' => __('Extra class name', 'AZEXO'),
                'param_name' => 'el_class',
                'description' => __('If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'AZEXO'),
            ),
            array(
                'type' => 'css_editor',
                'heading' => __('Css', 'AZEXO'),
                'param_name' => 'css',
                'group' => __('Design options', 'AZEXO'),
            ),
        )
    ));

    class WPBakeryShortCode_azexo_post_field extends WPBakeryShortCode {
        
    }

    global $azexo_fields;
    if (!isset($azexo_fields)) {
        $azexo_fields = array();
    }
    vc_map(array(
        "name" => "AZEXO - Post field",
        "base" => "azexo_post_field",
        'category' => __('AZEXO', 'AZEXO'),
        "content_element" => true,
        "controls" => "full",
        "show_settings_on_create" => true,
        'params' => array(
            array(
                'type' => 'textfield',
                'heading' => __('Post ID', 'AZEXO'),
                'param_name' => 'post_id',
                'description' => __('Post ID', 'AZEXO'),
            ),
            array(
                'type' => 'dropdown',
                'heading' => __('Post field', 'AZEXO'),
                'param_name' => 'field',
                'value' => array_merge(array(__('None', 'AZEXO') => ''), array_flip($azexo_fields)),
                'description' => __('Post field.', 'AZEXO'),
                'admin_label' => true
            ),
            array(
                'type' => 'textfield',
                'heading' => __('Extra class name', 'AZEXO'),
                'param_name' => 'el_class',
                'description' => __('If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'AZEXO'),
            ),
            array(
                'type' => 'css_editor',
                'heading' => __('Css', 'AZEXO'),
                'param_name' => 'css',
                'group' => __('Design options', 'AZEXO'),
            ),
        )
    ));

    class WPBakeryShortCode_azexo_posts_list extends WPBakeryShortCode {
        
    }

    $post_types = get_post_types(array(), 'objects');
    $post_types_list = array();
    if (is_array($post_types) && !empty($post_types)) {
        foreach ($post_types as $slug => $post_type) {
            if ($slug !== 'revision' && $slug !== 'nav_menu_item'/* && $slug !== 'attachment' */) {
                $post_types_list[] = array($slug, $post_type->label);
            }
        }
    }
    $post_types_list[] = array('custom', __('Custom query', 'AZEXO'));
    $post_types_list[] = array('ids', __('List of IDs', 'AZEXO'));

    $loop_params = array(
        array(
            'type' => 'dropdown',
            'heading' => __('Data source', 'AZEXO'),
            'param_name' => 'post_type',
            'value' => $post_types_list,
            'description' => __('Select content type for your list.', 'AZEXO')
        ),
        array(
            'type' => 'autocomplete',
            'heading' => __('Include only', 'AZEXO'),
            'param_name' => 'include',
            'description' => __('Add posts, pages, etc. by title.', 'AZEXO'),
            'settings' => array(
                'multiple' => true,
                'sortable' => true,
                'groups' => true,
            ),
            'dependency' => array(
                'element' => 'post_type',
                'value' => array('ids'),
            ),
        ),
        // Custom query tab
        array(
            'type' => 'textarea_safe',
            'heading' => __('Custom query', 'AZEXO'),
            'param_name' => 'custom_query',
            'description' => __('Build custom query according to <a href="http://codex.wordpress.org/Function_Reference/query_posts">WordPress Codex</a>.', 'AZEXO'),
            'dependency' => array(
                'element' => 'post_type',
                'value' => array('custom'),
            ),
        ),
        array(
            'type' => 'autocomplete',
            'heading' => __('Narrow data source', 'AZEXO'),
            'param_name' => 'taxonomies',
            'settings' => array(
                'multiple' => true,
                // is multiple values allowed? default false
                // 'sortable' => true, // is values are sortable? default false
                'min_length' => 1,
                // min length to start search -> default 2
                // 'no_hide' => true, // In UI after select doesn't hide an select list, default false
                'groups' => true,
                // In UI show results grouped by groups, default false
                'unique_values' => true,
                // In UI show results except selected. NB! You should manually check values in backend, default false
                'display_inline' => true,
                // In UI show results inline view, default false (each value in own line)
                'delay' => 500,
                // delay for search. default 500
                'auto_focus' => true,
            // auto focus input, default true
            // 'values' => $taxonomies_list,
            ),
            'param_holder_class' => 'vc_not-for-custom',
            'description' => __('Enter categories, tags or custom taxonomies.', 'AZEXO'),
            'dependency' => array(
                'element' => 'post_type',
                'value_not_equal_to' => array('ids', 'custom'),
            ),
        ),
        array(
            'type' => 'textfield',
            'heading' => __('Total items', 'AZEXO'),
            'param_name' => 'max_items',
            'value' => 10, // default value
            'param_holder_class' => 'vc_not-for-custom',
            'description' => __('Set max limit for items in list or enter -1 to display all (limited to 1000).', 'AZEXO'),
            'dependency' => array(
                'element' => 'post_type',
                'value_not_equal_to' => array('ids', 'custom'),
            ),
        ),
        // Data settings
        array(
            'type' => 'dropdown',
            'heading' => __('Order by', 'AZEXO'),
            'param_name' => 'orderby',
            'value' => array(
                __('Date', 'AZEXO') => 'date',
                __('Order by post ID', 'AZEXO') => 'ID',
                __('Author', 'AZEXO') => 'author',
                __('Title', 'AZEXO') => 'title',
                __('Last modified date', 'AZEXO') => 'modified',
                __('Post/page parent ID', 'AZEXO') => 'parent',
                __('Number of comments', 'AZEXO') => 'comment_count',
                __('Menu order/Page Order', 'AZEXO') => 'menu_order',
                __('Meta value', 'AZEXO') => 'meta_value',
                __('Meta value number', 'AZEXO') => 'meta_value_num',
                // __('Matches same order you passed in via the 'include' parameter.', 'AZEXO') => 'post__in'
                __('Random order', 'AZEXO') => 'rand',
            ),
            'description' => __('Select order type. If "Meta value" or "Meta value Number" is chosen then meta key is required.', 'AZEXO'),
            'group' => __('Data Settings', 'AZEXO'),
            'param_holder_class' => 'vc_grid-data-type-not-ids',
            'dependency' => array(
                'element' => 'post_type',
                'value_not_equal_to' => array('ids', 'custom'),
            ),
        ),
        array(
            'type' => 'dropdown',
            'heading' => __('Sorting', 'AZEXO'),
            'param_name' => 'order',
            'group' => __('Data Settings', 'AZEXO'),
            'value' => array(
                __('Descending', 'AZEXO') => 'DESC',
                __('Ascending', 'AZEXO') => 'ASC',
            ),
            'param_holder_class' => 'vc_grid-data-type-not-ids',
            'description' => __('Select sorting order.', 'AZEXO'),
            'dependency' => array(
                'element' => 'post_type',
                'value_not_equal_to' => array('ids', 'custom'),
            ),
        ),
        array(
            'type' => 'textfield',
            'heading' => __('Meta key', 'AZEXO'),
            'param_name' => 'meta_key',
            'description' => __('Input meta key for list ordering.', 'AZEXO'),
            'group' => __('Data Settings', 'AZEXO'),
            'param_holder_class' => 'vc_grid-data-type-not-ids',
            'dependency' => array(
                'element' => 'orderby',
                'value' => array('meta_value', 'meta_value_num'),
            ),
        ),
        array(
            'type' => 'textfield',
            'heading' => __('Offset', 'AZEXO'),
            'param_name' => 'offset',
            'description' => __('Number of list elements to displace or pass over.', 'AZEXO'),
            'group' => __('Data Settings', 'AZEXO'),
            'param_holder_class' => 'vc_grid-data-type-not-ids',
            'dependency' => array(
                'element' => 'post_type',
                'value_not_equal_to' => array('ids', 'custom'),
            ),
        ),
        array(
            'type' => 'autocomplete',
            'heading' => __('Exclude', 'AZEXO'),
            'param_name' => 'exclude',
            'description' => __('Exclude posts, pages, etc. by title.', 'AZEXO'),
            'group' => __('Data Settings', 'AZEXO'),
            'settings' => array(
                'multiple' => true,
            ),
            'param_holder_class' => 'vc_grid-data-type-not-ids',
            'dependency' => array(
                'element' => 'post_type',
                'value_not_equal_to' => array('ids', 'custom'),
                'callback' => 'vc_grid_exclude_dependency_callback',
            ),
        ),
    );
    add_filter('vc_autocomplete_azexo_posts_list_include_callback', 'vc_include_field_search', 10, 1); // Get suggestion(find). Must return an array
    add_filter('vc_autocomplete_azexo_posts_list_include_render', 'vc_include_field_render', 10, 1); // Render exact product. Must return an array (label,value)
    add_filter('vc_autocomplete_azexo_posts_list_taxonomies_callback', 'vc_autocomplete_taxonomies_field_search', 10, 1);
    add_filter('vc_autocomplete_azexo_posts_list_taxonomies_render', 'vc_autocomplete_taxonomies_field_render', 10, 1);
    add_filter('vc_autocomplete_azexo_posts_list_exclude_callback', 'vc_exclude_field_search', 10, 1); // Get suggestion(find). Must return an array
    add_filter('vc_autocomplete_azexo_posts_list_exclude_render', 'vc_exclude_field_render', 10, 1); // Render exact product. Must return an array (label,value)


    vc_map(array(
        "name" => "AZEXO - Posts List",
        "base" => "azexo_posts_list",
        'category' => __('AZEXO', 'AZEXO'),
        "content_element" => true,
        "controls" => "full",
        "show_settings_on_create" => true,
        'params' => array_merge($loop_params, array(
            array(
                'type' => 'textfield',
                'heading' => __('List title', 'AZEXO'),
                'param_name' => 'title',
                'description' => __('Enter text which will be used as title. Leave blank if no title is needed.', 'AZEXO'),
                'admin_label' => true
            ),
            array(
                'type' => 'textfield',
                'heading' => __('Posts clauses filter function name', 'AZEXO'),
                'param_name' => 'posts_clauses',
                'description' => __('Function which can alter WP_Query object.', 'AZEXO')
            ),
            array(
                'type' => 'checkbox',
                'heading' => __('Only content?', 'AZEXO'),
                'param_name' => 'only_content',
                'value' => array(__('Yes, please', 'AZEXO') => 'yes')
            ),
            array(
                'type' => 'dropdown',
                'heading' => __('Post template', 'AZEXO'),
                'param_name' => 'template',
                'value' => array_merge(array(__('Default', 'AZEXO') => 'post'), array_flip($azexo_templates)),
                'description' => __('Post template.', 'AZEXO'),
                'dependency' => array(
                    'element' => 'only_content',
                    'is_empty' => true,
                ),
            ),
            array(
                'type' => 'checkbox',
                'heading' => __('Show as carousel?', 'AZEXO'),
                'param_name' => 'carousel',
                'value' => array(__('Yes, please', 'AZEXO') => 'yes')
            ),
            array(
                'type' => 'textfield',
                'heading' => __('Item margin', 'AZEXO'),
                'param_name' => 'item_margin',
                'value' => '0',
                'dependency' => array(
                    'element' => 'carousel',
                    'not_empty' => true,
                )),
            array(
                'type' => 'checkbox',
                'heading' => __('Center item?', 'AZEXO'),
                'param_name' => 'center',
                'value' => array(__('Yes, please', 'AZEXO') => 'yes'),
                'dependency' => array(
                    'element' => 'carousel',
                    'not_empty' => true,
                )),
            array(
                'type' => 'textfield',
                'heading' => __('Posts per carousel item', 'AZEXO'),
                'param_name' => 'posts_per_item',
                'value' => '1',
                'dependency' => array(
                    'element' => 'carousel',
                    'not_empty' => true,
                )),
            array(
                'type' => 'checkbox',
                'heading' => __('Full width', 'AZEXO'),
                'param_name' => 'full_width',
                'value' => array(__('Yes, please', 'AZEXO') => 'yes'),
                'dependency' => array(
                    'element' => 'carousel',
                    'not_empty' => true,
                )),
            array(
                'type' => 'textfield',
                'heading' => __('Extra class name', 'AZEXO'),
                'param_name' => 'el_class',
                'description' => __('If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'AZEXO'),
            ),
            array(
                'type' => 'css_editor',
                'heading' => __('Css', 'AZEXO'),
                'param_name' => 'css',
                'group' => __('Design options', 'AZEXO'),
            ),
        ))
    ));

    class WPBakeryShortCode_azexo_panel extends WPBakeryShortCodesContainer {
        
    }

    vc_map(array(
        "name" => "AZEXO - Panel",
        "base" => "azexo_panel",
        'category' => __('AZEXO', 'AZEXO'),
        "as_parent" => array('except' => 'azexo_panel'),
        "content_element" => true,
        "controls" => "full",
        "show_settings_on_create" => true,
        //"is_container" => true,
        'params' => array(
            array(
                'type' => 'textfield',
                'heading' => __('Panel title', 'AZEXO'),
                'param_name' => 'title',
                'description' => __('Enter text which will be used as title. Leave blank if no title is needed.', 'AZEXO'),
                'admin_label' => true
            ),
            array(
                'type' => 'vc_link',
                'heading' => __('URL (Link)', 'AZEXO'),
                'param_name' => 'link',
                'dependency' => array(
                    'element' => 'title',
                    'not_empty' => true,
                ),
            ),            
            array(
                'type' => 'textfield',
                'heading' => __('Extra class name', 'AZEXO'),
                'param_name' => 'el_class',
                'description' => __('If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'AZEXO'),
            ),
            array(
                'type' => 'css_editor',
                'heading' => __('Css', 'AZEXO'),
                'param_name' => 'css',
                'group' => __('Design options', 'AZEXO'),
            ),
        ),
        "js_view" => 'VcColumnView'
    ));

    class WPBakeryShortCode_azexo_taxonomy extends WPBakeryShortCode {
        
    }

    $taxonomies = get_taxonomies(array(), 'objects');
    $taxonomy_options = array();
    foreach ($taxonomies as $slug => $taxonomy) {
        $taxonomy_options[$taxonomy->label] = $slug;
    }
    vc_map(array(
        'name' => "AZEXO - Taxonomy",
        'base' => 'azexo_taxonomy',
        'category' => __('AZEXO', 'AZEXO'),
        'description' => __('A list or dropdown of categories', 'AZEXO'),
        'params' => array(
            array(
                'type' => 'textfield',
                'heading' => __('Widget title', 'AZEXO'),
                'param_name' => 'title',
                'description' => __('What text use as a widget title. Leave blank to use default widget title.', 'AZEXO'),
                'value' => __('Categories', 'AZEXO'),
            ),
            array(
                'type' => 'dropdown',
                'heading' => __('Taxonomy', 'AZEXO'),
                'param_name' => 'taxonomy',
                'value' => array_merge(array(__('Select', 'AZEXO') => ''), $taxonomy_options),
            ),
            array(
                'type' => 'checkbox',
                'heading' => __('Display options', 'AZEXO'),
                'param_name' => 'options',
                'value' => array(
                    __('Dropdown', 'AZEXO') => 'dropdown',
                    __('Show post counts', 'AZEXO') => 'count',
                    __('Show hierarchy', 'AZEXO') => 'hierarchical'
                ),
                'description' => __('Select display options for categories.', 'AZEXO')
            ),
            array(
                'type' => 'textfield',
                'heading' => __('Extra class name', 'AZEXO'),
                'param_name' => 'el_class',
                'description' => __('Style particular content element differently - add a class name and refer to it in custom CSS.', 'AZEXO')
            )
        )
    ));

    class WPBakeryShortCode_azexo_carousel extends WPBakeryShortCodesContainer {
        
    }

    vc_map(array(
        "name" => "AZEXO - Carousel",
        "base" => "azexo_carousel",
        'category' => __('AZEXO', 'AZEXO'),
        "as_parent" => array('only' => 'azexo_generic_content'),
        "content_element" => true,
        "controls" => "full",
        "show_settings_on_create" => true,
        //"is_container" => true,
        'params' => array(
            array(
                'type' => 'textfield',
                'heading' => __('Carousel title', 'AZEXO'),
                'param_name' => 'title',
                'description' => __('Enter text which will be used as title. Leave blank if no title is needed.', 'AZEXO')
            ),
            array(
                'type' => 'textfield',
                'heading' => __('Item margin', 'AZEXO'),
                'param_name' => 'item_margin',
                'value' => '0',
            ),
            array(
                'type' => 'checkbox',
                'heading' => __('Center item?', 'AZEXO'),
                'param_name' => 'center',
                'value' => array(__('Yes, please', 'AZEXO') => 'yes'),
            ),
            array(
                'type' => 'textfield',
                'heading' => __('Contents per carousel item', 'AZEXO'),
                'param_name' => 'contents_per_item',
                'value' => '1',
            ),
            array(
                'type' => 'param_group',
                'heading' => __('Responsive', 'AZEXO'),
                'param_name' => 'responsive',
                'value' => urlencode(json_encode(array(
                    array(
                        'window_width' => '0',
                        'items' => '1'
                    ),
                    array(
                        'window_width' => '768',
                        'items' => '1'
                    )
                ))),
                'params' => array(
                    array(
                        'type' => 'textfield',
                        'heading' => __('Window width', 'AZEXO'),
                        'param_name' => 'window_width',
                        'admin_label' => true
                    ),
                    array(
                        'type' => 'textfield',
                        'heading' => __('Items', 'AZEXO'),
                        'param_name' => 'items',
                        'admin_label' => true
                    ),
                ),
            ),
            array(
                'type' => 'textfield',
                'heading' => __('Extra class name', 'AZEXO'),
                'param_name' => 'el_class',
                'description' => __('If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'AZEXO'),
            ),
            array(
                'type' => 'css_editor',
                'heading' => __('Css', 'AZEXO'),
                'param_name' => 'css',
                'group' => __('Design options', 'AZEXO'),
            ),
        ),
        "js_view" => 'VcColumnView'
    ));

    class WPBakeryShortCode_azexo_generic_content extends WPBakeryShortCode {
        
    }

    vc_map(array(
        "name" => "AZEXO - Generic Content",
        "base" => "azexo_generic_content",
        'category' => __('AZEXO', 'AZEXO'),
        "controls" => "full",
        "show_settings_on_create" => true,
        'params' => array(
            array(
                'type' => 'dropdown',
                'heading' => __('Media type', 'AZEXO'),
                'param_name' => 'media_type',
                'admin_label' => true,
                'value' => array(
                    __('No media', 'AZEXO') => 'no_media',
                    __('Image', 'AZEXO') => 'image',
                    __('Gallery', 'AZEXO') => 'gallery',
                    __('Video', 'AZEXO') => 'video',
                    __('Icon', 'AZEXO') => 'icon',
                    __('Image and Icon', 'AZEXO') => 'image_icon',
                ),
                'group' => __('Media', 'AZEXO'),
            ),
            array(
                'type' => 'attach_image',
                'heading' => __('Image', 'AZEXO'),
                'param_name' => 'image',
                'group' => __('Media', 'AZEXO'),
                'dependency' => array(
                    'element' => 'media_type',
                    'value' => array('image', 'image_icon'),
                ),
            ),
            array(
                'type' => 'attach_images',
                'heading' => __('Images', 'AZEXO'),
                'param_name' => 'gallery',
                'group' => __('Media', 'AZEXO'),
                'dependency' => array(
                    'element' => 'media_type',
                    'value' => array('gallery'),
                ),
            ),
            array(
                'type' => 'checkbox',
                'heading' => __('Thumbnails?', 'AZEXO'),
                'param_name' => 'thumbnails',
                'group' => __('Media', 'AZEXO'),
                'value' => array(__('Yes, please', 'AZEXO') => 'yes'),
                'dependency' => array(
                    'element' => 'media_type',
                    'value' => array('gallery'),
                )),
            array(
                'type' => 'textfield',
                'heading' => __('Image size', 'AZEXO'),
                'param_name' => 'img_size',
                'group' => __('Media', 'AZEXO'),
                'value' => 'thumbnail',
                'description' => __('Enter image size (Example: "thumbnail", "medium", "large", "full" or other sizes defined by theme). Alternatively enter size in pixels (Example: 200x100 (Width x Height)). Leave parameter empty to use "thumbnail" by default.', 'AZEXO'),
                'dependency' => array(
                    'element' => 'media_type',
                    'value' => array('image', 'image_icon', 'gallery'),
                ),
            ),
            array(
                'type' => 'textfield',
                'heading' => __('Video link', 'AZEXO'),
                'param_name' => 'video',
                'group' => __('Media', 'AZEXO'),
                'value' => 'http://vimeo.com/92033601',
                'description' => sprintf(__('Enter link to video (Note: read more about available formats at WordPress <a href="%s" target="_blank">codex page</a>).', 'AZEXO'), 'http://codex.wordpress.org/Embeds#Okay.2C_So_What_Sites_Can_I_Embed_From.3F'),
                'dependency' => array(
                    'element' => 'media_type',
                    'value' => array('video'),
                ),
            ),
            array(
                'type' => 'dropdown',
                'heading' => __('Icon library', 'AZEXO'),
                'value' => array(
                    __('Font Awesome', 'AZEXO') => 'fontawesome',
                    __('Open Iconic', 'AZEXO') => 'openiconic',
                    __('Typicons', 'AZEXO') => 'typicons',
                    __('Entypo', 'AZEXO') => 'entypo',
                    __('Linecons', 'AZEXO') => 'linecons',
                ),
                'param_name' => 'icon_library',
                'group' => __('Media', 'AZEXO'),
                'description' => __('Select icon library.', 'AZEXO'),
                'dependency' => array(
                    'element' => 'media_type',
                    'value' => array('icon', 'image_icon'),
                ),
            ),
            array(
                'type' => 'iconpicker',
                'heading' => __('Icon', 'AZEXO'),
                'param_name' => 'icon_fontawesome',
                'group' => __('Media', 'AZEXO'),
                'value' => 'fa fa-adjust', // default value to backend editor admin_label
                'settings' => array(
                    'emptyIcon' => false,
                    // default true, display an "EMPTY" icon?
                    'iconsPerPage' => 4000,
                // default 100, how many icons per/page to display, we use (big number) to display all icons in single page
                ),
                'dependency' => array(
                    'element' => 'icon_library',
                    'value' => 'fontawesome',
                ),
                'description' => __('Select icon from library.', 'AZEXO'),
            ),
            array(
                'type' => 'iconpicker',
                'heading' => __('Icon', 'AZEXO'),
                'param_name' => 'icon_openiconic',
                'group' => __('Media', 'AZEXO'),
                'value' => 'vc-oi vc-oi-dial', // default value to backend editor admin_label
                'settings' => array(
                    'emptyIcon' => false, // default true, display an "EMPTY" icon?
                    'type' => 'openiconic',
                    'iconsPerPage' => 4000, // default 100, how many icons per/page to display
                ),
                'dependency' => array(
                    'element' => 'icon_library',
                    'value' => 'openiconic',
                ),
                'description' => __('Select icon from library.', 'AZEXO'),
            ),
            array(
                'type' => 'iconpicker',
                'heading' => __('Icon', 'AZEXO'),
                'param_name' => 'icon_typicons',
                'group' => __('Media', 'AZEXO'),
                'value' => 'typcn typcn-adjust-brightness', // default value to backend editor admin_label
                'settings' => array(
                    'emptyIcon' => false, // default true, display an "EMPTY" icon?
                    'type' => 'typicons',
                    'iconsPerPage' => 4000, // default 100, how many icons per/page to display
                ),
                'dependency' => array(
                    'element' => 'icon_library',
                    'value' => 'typicons',
                ),
                'description' => __('Select icon from library.', 'AZEXO'),
            ),
            array(
                'type' => 'iconpicker',
                'heading' => __('Icon', 'AZEXO'),
                'param_name' => 'icon_entypo',
                'group' => __('Media', 'AZEXO'),
                'value' => 'entypo-icon entypo-icon-note', // default value to backend editor admin_label
                'settings' => array(
                    'emptyIcon' => false, // default true, display an "EMPTY" icon?
                    'type' => 'entypo',
                    'iconsPerPage' => 4000, // default 100, how many icons per/page to display
                ),
                'dependency' => array(
                    'element' => 'icon_library',
                    'value' => 'entypo',
                ),
            ),
            array(
                'type' => 'iconpicker',
                'heading' => __('Icon', 'AZEXO'),
                'param_name' => 'icon_linecons',
                'group' => __('Media', 'AZEXO'),
                'value' => 'vc_li vc_li-heart', // default value to backend editor admin_label
                'settings' => array(
                    'emptyIcon' => false, // default true, display an "EMPTY" icon?
                    'type' => 'linecons',
                    'iconsPerPage' => 4000, // default 100, how many icons per/page to display
                ),
                'dependency' => array(
                    'element' => 'icon_library',
                    'value' => 'linecons',
                ),
                'description' => __('Select icon from library.', 'AZEXO'),
            ),
            array(
                'type' => 'dropdown',
                'heading' => __('Link click effect', 'AZEXO'),
                'value' => array(
                    __('Classic link', 'AZEXO') => 'classic',
                    __('Image popup', 'AZEXO') => 'image_popup',
                    __('IFrame  popup', 'AZEXO') => 'iframe_popup',
                ),
                'param_name' => 'media_link_click',
                'group' => __('Media', 'AZEXO'),
                'dependency' => array(
                    'element' => 'media_type',
                    'value' => array('image', 'icon', 'image_icon'),
                ),
            ),
            array(
                'type' => 'vc_link',
                'heading' => __('URL (Link)', 'AZEXO'),
                'param_name' => 'media_link',
                'group' => __('Media', 'AZEXO'),
                'dependency' => array(
                    'element' => 'media_type',
                    'value' => array('image', 'icon', 'image_icon'),
                ),
            ),
            array(
                'type' => 'textarea_raw_html',
                'heading' => __('Extra', 'AZEXO'),
                'param_name' => 'extra',
                'group' => __('Header', 'AZEXO'),
            ),
            array(
                'type' => 'dropdown',
                'heading' => __('Link click effect', 'AZEXO'),
                'value' => array(
                    __('Classic link', 'AZEXO') => 'classic',
                    __('Image popup', 'AZEXO') => 'image_popup',
                    __('IFrame  popup', 'AZEXO') => 'iframe_popup',
                ),
                'param_name' => 'title_link_click',
                'group' => __('Header', 'AZEXO'),
            ),
            array(
                'type' => 'vc_link',
                'heading' => __('URL (Link)', 'AZEXO'),
                'param_name' => 'title_link',
                'group' => __('Header', 'AZEXO'),
            ),
            array(
                'type' => 'textfield',
                'heading' => __('Title', 'AZEXO'),
                'param_name' => 'title',
                'admin_label' => true,
                'group' => __('Header', 'AZEXO'),
            ),
            array(
                'type' => 'textarea_raw_html',
                'heading' => __('Meta', 'AZEXO'),
                'param_name' => 'meta',
                'group' => __('Header', 'AZEXO'),
            ),
            array(
                'type' => 'textarea_html',
                'heading' => __('Content', 'AZEXO'),
                'holder' => 'div',
                'param_name' => 'content',
                'group' => __('Content', 'AZEXO'),
                'value' => __('<p>I am text block. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.</p>', 'AZEXO')
            ),
            array(
                'type' => 'dropdown',
                'heading' => __('Link click effect', 'AZEXO'),
                'value' => array(
                    __('Classic link', 'AZEXO') => 'classic',
                    __('Image popup', 'AZEXO') => 'image_popup',
                    __('IFrame  popup', 'AZEXO') => 'iframe_popup',
                ),
                'param_name' => 'footer_link_click',
                'group' => __('Footer', 'AZEXO'),
            ),
            array(
                'type' => 'vc_link',
                'heading' => __('URL (Link)', 'AZEXO'),
                'param_name' => 'footer_link',
                'group' => __('Footer', 'AZEXO'),
            ),
            array(
                'type' => 'textarea_raw_html',
                'heading' => __('Footer', 'AZEXO'),
                'param_name' => 'footer',
                'group' => __('Footer', 'AZEXO'),
            ),
            array(
                'type' => 'textfield',
                'heading' => __('Scroll Reveal settings', 'AZEXO'),
                'param_name' => 'sr',
            ),
            array(
                'type' => 'textfield',
                'heading' => __('Extra class name', 'AZEXO'),
                'param_name' => 'el_class',
                'description' => __('If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'AZEXO'),
            ),
            array(
                'type' => 'css_editor',
                'heading' => __('Css', 'AZEXO'),
                'param_name' => 'css',
                'group' => __('Design options', 'AZEXO'),
            ),
        ),
    ));
    $directory_iterator = new DirectoryIterator(get_template_directory() . '/azexo/icons');
    foreach ($directory_iterator as $fileInfo) {
        if ($fileInfo->isFile() && $fileInfo->getExtension() == 'php') {
            require_once($fileInfo->getPathname());
        }
    }
}