<?php
/*
  ReduxFramework Config File
 */

if (!class_exists('AZEXO_Redux_Framework_config')) {

    class AZEXO_Redux_Framework_config {

        public $args = array();
        public $sections = array();
        public $theme;
        public $ReduxFramework;

        public function __construct() {

            if (!class_exists('ReduxFramework')) {
                return;
            }
            if (true == Redux_Helpers::isTheme(__FILE__)) {
                $this->initSettings();
            } else {
                add_action('plugins_loaded', array($this, 'initSettings'), 10);
            }
        }

        public function initSettings() {
            $this->theme = wp_get_theme();
            $this->setArguments();
            $this->setSections();

            if (!isset($this->args['opt_name'])) { // No errors please
                return;
            }
            add_action('redux/loaded', array($this, 'remove_demo'));
            add_filter('redux/options/' . $this->args['opt_name'] . '/args', array($this, 'change_arguments'));
            $this->ReduxFramework = new ReduxFramework($this->sections, $this->args);
        }

        function change_arguments($args) {
            $args['dev_mode'] = false;

            return $args;
        }

        function remove_demo() {
            if (class_exists('ReduxFrameworkPlugin')) {
                remove_filter('plugin_row_meta', array(ReduxFrameworkPlugin::instance(), 'plugin_metalinks'), null, 2);
                remove_action('admin_notices', array(ReduxFrameworkPlugin::instance(), 'admin_notices'));
            }
        }

        public function setSections() {

            ob_start();

            $ct = wp_get_theme();
            $this->theme = $ct;
            $item_name = $this->theme->get('Name');
            $tags = $this->theme->Tags;
            $screenshot = $this->theme->get_screenshot();
            $class = $screenshot ? 'has-screenshot' : '';

            $customize_title = sprintf(__('Customize &#8220;%s&#8221;', 'AZEXO'), $this->theme->display('Name'));
            ?>
            <div id="current-theme" class="<?php echo esc_attr($class); ?>">
                <?php if ($screenshot) : ?>
                    <?php if (current_user_can('edit_theme_options')) : ?>
                        <a href="<?php echo wp_customize_url(); ?>" class="load-customize hide-if-no-customize" title="<?php echo esc_attr($customize_title); ?>">
                            <img src="<?php echo esc_url($screenshot); ?>" alt="<?php esc_attr_e('Current theme preview', 'AZEXO'); ?>" />
                        </a>
                    <?php endif; ?>
                    <img class="hide-if-customize" src="<?php echo esc_url($screenshot); ?>" alt="<?php esc_attr_e('Current theme preview', 'AZEXO'); ?>" />
                <?php endif; ?>

                <h4><?php echo $this->theme->display('Name'); ?></h4>

                <div>
                    <ul class="theme-info">
                        <li><?php printf(__('By %s', 'AZEXO'), $this->theme->display('Author')); ?></li>
                        <li><?php printf(__('Version %s', 'AZEXO'), $this->theme->display('Version')); ?></li>
                        <li><?php echo '<strong>' . __('Tags', 'AZEXO') . ':</strong> '; ?><?php printf($this->theme->display('Tags')); ?></li>
                    </ul>
                    <p class="theme-description"><?php echo $this->theme->display('Description'); ?></p>
                    <?php
                    if ($this->theme->parent()) {
                        printf(' <p class="howto">' . __('This <a href="%1$s">child theme</a> requires its parent theme, %2$s.', 'AZEXO') . '</p>', __('http://codex.wordpress.org/Child_Themes', 'AZEXO'), $this->theme->parent()->display('Name'));
                    }
                    ?>

                </div>
            </div>

            <?php
            $item_info = ob_get_contents();

            ob_end_clean();

            $options = get_option(AZEXO_THEME_NAME);
            global $azexo_templates;
            if (!isset($azexo_templates)) {
                $azexo_templates = array();
            }
            $azexo_templates = array_merge($azexo_templates, array(
                'post' => __('Post', 'AZEXO'),
                'half_image_post' => __('Half image post', 'AZEXO'),
                'bg_image_post' => __('Background image post', 'AZEXO'),
                'masonry_post' => __('Masonry post', 'AZEXO'),
                'related_post' => __('Related post', 'AZEXO'),
                'carousel_post' => __('Carousel post', 'AZEXO'),
                'widget_post' => __('Widget post', 'AZEXO'),
                'testimonial_post' => __('Testimonial post', 'AZEXO'),
                'thumb_post' => __('Thumbnail post', 'AZEXO'),
                'title_post' => __('Title-post', 'AZEXO'),
                'thumb_title_post' => __('Thumb/title-post', 'AZEXO'),
                'big_thumb_title_post' => __('Big-thumb/title-post', 'AZEXO'),
                'avatar_title_post' => __('Avatar/title-post', 'AZEXO'),
            ));

            if (isset($options['templates']) && is_array($options['templates'])) {
                $options['templates'] = array_filter($options['templates']);
                if (!empty($options['templates'])) {
                    $azexo_templates = array_merge($azexo_templates, array_combine(array_map('sanitize_title', $options['templates']), $options['templates']));
                }
            }

            $azexo_templates = apply_filters('azexo_templates', $azexo_templates);

            global $azexo_fields;
            if (!isset($azexo_fields)) {
                $azexo_fields = array();
            }
            global $azexo_post_fields;
            $azexo_post_fields = array(
                'post_title' => __('Post title', 'AZEXO'),
                'post_summary' => __('Post summary', 'AZEXO'),
                'post_content' => __('Post content', 'AZEXO'),
                'post_sticky' => __('Post sticky', 'AZEXO'),
                'post_date' => __('Post date', 'AZEXO'),
                'post_splitted_date' => __('Post splitted date', 'AZEXO'),
                'post_author' => __('Post author', 'AZEXO'),
                'post_author_avatar' => __('Post author avatar', 'AZEXO'),
                'post_category' => __('Post category', 'AZEXO'),
                'post_tags' => __('Post tags', 'AZEXO'),
                'post_like' => __('Post like', 'AZEXO'),
                'post_last_comment' => __('Post last comment', 'AZEXO'),
                'post_last_comment_author' => __('Post last comment author', 'AZEXO'),
                'post_last_comment_date' => __('Post last comment date', 'AZEXO'),
                'post_comments_count' => __('Post comments count', 'AZEXO'),
            );

            $azexo_fields = array_merge($azexo_fields, $azexo_post_fields);

            $azexo_fields = array_merge($azexo_fields, azexo_get_field_templates());


            if (isset($options['meta_fields']) && is_array($options['meta_fields'])) {
                $options['meta_fields'] = array_filter($options['meta_fields']);
                if (!empty($options['meta_fields'])) {
                    $azexo_fields = array_merge($azexo_fields, array_combine($options['meta_fields'], $options['meta_fields']));
                }
            }

            $azexo_fields = apply_filters('azexo_fields', $azexo_fields);


            $general_settings_fields = array();
            if (class_exists('WPLessPlugin')) {
                $general_settings_fields[] = array(
                    'id' => 'brand-color',
                    'type' => 'color',
                    'title' => __('Brand color', 'AZEXO'),
                    'validate' => 'color',
                    'default' => '#000',
                );
                $general_settings_fields[] = array(
                    'id' => 'accent-1-color',
                    'type' => 'color',
                    'title' => __('Accent 1 color', 'AZEXO'),
                    'validate' => 'color',
                    'default' => '#000',
                );
                $general_settings_fields[] = array(
                    'id' => 'accent-2-color',
                    'type' => 'color',
                    'title' => __('Accent 2 color', 'AZEXO'),
                    'validate' => 'color',
                    'default' => '#000',
                );
            }
            $general_settings_fields[] = array(
                'id' => 'default_post_template',
                'type' => 'select',
                'title' => __('Default blog template', 'AZEXO'),
                'options' => $azexo_templates,
                'default' => 'post',
            );
            global $azwoo_templates;
            if (isset($azwoo_templates)) {
                $general_settings_fields[] = array(
                    'id' => 'default_product_template',
                    'type' => 'select',
                    'title' => __('Default shop template', 'AZEXO'),
                    'options' => $azexo_templates,
                    'default' => 'shop_product',
                );
            }
            $general_settings_fields[] = array(
                'id' => 'show_sidebar',
                'type' => 'select',
                'title' => __('Show sidebar', 'AZEXO'),
                'options' => array(
                    'hidden' => __('Hidden', 'AZEXO'),
                    'left' => __('Left side', 'AZEXO'),
                    'right' => __('Right side', 'AZEXO'),
                ),
                'default' => 'right',
            );
            $general_settings_fields[] = array(
                'id' => 'favicon',
                'type' => 'media',
                'title' => __('Favicon', 'AZEXO'),
                'subtitle' => __('Upload any media using the WordPress native uploader', 'AZEXO'),
            );
            $general_settings_fields[] = array(
                'id' => 'custom-css',
                'type' => 'ace_editor',
                'title' => __('CSS Code', 'AZEXO'),
                'subtitle' => __('Paste your CSS code here.', 'AZEXO'),
                'mode' => 'css',
                'theme' => 'monokai',
                'default' => "#header{\nmargin: 0 auto;\n}"
            );
            $general_settings_fields[] = array(
                'id' => 'custom-js',
                'type' => 'ace_editor',
                'title' => __('JS Code', 'AZEXO'),
                'subtitle' => __('Paste your JS code here.', 'AZEXO'),
                'mode' => 'javascript',
                'theme' => 'chrome',
                'default' => "jQuery(document).ready(function(){\n\n});"
            );

            if (isset($options['header']) && is_array($options['header']) && in_array('logo', $options['header'])) {
                array_unshift($general_settings_fields, array(
                    'id' => 'logo',
                    'type' => 'media',
                    'title' => __('Logo', 'AZEXO'),
                    'subtitle' => __('Upload any media using the WordPress native uploader', 'AZEXO'),
                ));
            }

            $skins = azexo_get_skins();

            // ACTUAL DECLARATION OF SECTIONS
            $this->sections[] = array(
                'icon' => 'el-icon-cogs',
                'title' => __('General settings', 'AZEXO'),
                'fields' => $general_settings_fields
            );

            $this->sections[] = array(
                'icon' => 'el-icon-cogs',
                'title' => __('Templates configuration', 'AZEXO'),
                'fields' => array(
                    array(
                        'id' => 'skin',
                        'type' => 'select',
                        'title' => __('Select skin', 'AZEXO'),
                        'options' => array_combine($skins, $skins),
                    ),
                    array(
                        'id' => 'header_sidebar_fullwidth',
                        'type' => 'checkbox',
                        'title' => __('Header sidebar fullwidth', 'AZEXO'),
                        'default' => '1'
                    ),
                    array(
                        'id' => 'middle_sidebar_fullwidth',
                        'type' => 'checkbox',
                        'title' => __('Middle sidebar fullwidth', 'AZEXO'),
                        'default' => '1'
                    ),
                    array(
                        'id' => 'footer_sidebar_fullwidth',
                        'type' => 'checkbox',
                        'title' => __('Footer sidebar fullwidth', 'AZEXO'),
                        'default' => '0'
                    ),
                    array(
                        'id' => 'show_page_title',
                        'type' => 'checkbox',
                        'title' => __('Show page title in templates', 'AZEXO'),
                        'default' => '0'
                    ),
                    array(
                        'id' => 'show_breadcrumbs',
                        'type' => 'checkbox',
                        'title' => __('Show breadcrumb in templates', 'AZEXO'),
                        'default' => '0'
                    ),
                    array(
                        'id' => 'header',
                        'type' => 'select',
                        'multi' => true,
                        'sortable' => true,
                        'title' => __('Header parts', 'AZEXO'),
                        'options' => array(
                            'logo' => __('Logo', 'AZEXO'),
                            'search' => __('Search', 'AZEXO'),
                            'primary_menu' => __('Primary menu', 'AZEXO'),
                            'secondary_menu' => __('Secondary menu', 'AZEXO'),
                            'mobile_menu_button' => __('Mobile menu button', 'AZEXO'),
                            'mobile_menu' => __('Mobile menu', 'AZEXO'),
                        ),
                        'default' => 'primary_menu',
                    ),
                    array(
                        'id' => 'author_bio',
                        'type' => 'checkbox',
                        'title' => __('Show author bio', 'AZEXO'),
                        'default' => '0'
                    ),
                    array(
                        'id' => 'post_navigation',
                        'type' => 'select',
                        'title' => __('Post navigation place', 'AZEXO'),
                        'options' => array(
                            'hidden' => __('Hidden', 'AZEXO'),
                            'before' => __('Before content', 'AZEXO'),
                            'after' => __('After content', 'AZEXO'),
                        ),
                        'default' => 'hidden',
                    ),
                    array(
                        'id' => 'post_navigation_previous',
                        'type' => 'text',
                        'title' => __('Post navigation previous text', 'AZEXO'),
                        'default' => '',
                    ),
                    array(
                        'id' => 'post_navigation_next',
                        'type' => 'text',
                        'title' => __('Post navigation next text', 'AZEXO'),
                        'default' => '',
                    ),
                    array(
                        'id' => 'default_title',
                        'type' => 'text',
                        'title' => __('Default page title', 'AZEXO'),
                        'default' => 'Latest posts',
                    ),
                    array(
                        'id' => 'post_page_title',
                        'type' => 'select',
                        'title' => __('Post page title', 'AZEXO'),
                        'options' => $azexo_fields,
                        'default' => '',
                    ),
                    array(
                        'id' => 'strip_excerpt',
                        'type' => 'checkbox',
                        'title' => __('Strip excerpt', 'AZEXO'),
                        'default' => '1',
                    ),
                    array(
                        'id' => 'excerpt_length',
                        'type' => 'text',
                        'title' => __('Excerpt length', 'AZEXO'),
                        'default' => '15',
                    ),
                    array(
                        'id' => 'comment_excerpt_length',
                        'type' => 'text',
                        'title' => __('Comment excerpt length', 'AZEXO'),
                        'default' => '15',
                    ),
                    array(
                        'id' => 'author_avatar_size',
                        'type' => 'text',
                        'title' => __('Author avatar size', 'AZEXO'),
                        'default' => '100',
                    ),
                    array(
                        'id' => 'avatar_size',
                        'type' => 'text',
                        'title' => __('Avatar size', 'AZEXO'),
                        'default' => '60',
                    ),
                    array(
                        'id' => 'templates',
                        'type' => 'multi_text',
                        'title' => __('Templates', 'AZEXO'),
                    ),
                    array(
                        'id' => 'meta_fields',
                        'type' => 'multi_text',
                        'title' => __('Meta fields', 'AZEXO'),
                    ),
                )
            );

            foreach ($azexo_templates as $template_slug => $template_name) {


                $places = array(
                    $template_slug . '_thumbnail' => $template_name . ' ' . __('thumbnail', 'AZEXO'),
                    $template_slug . '_extra' => $template_name . ' ' . __('extra', 'AZEXO'),
                    $template_slug . '_meta' => $template_name . ' ' . __('meta', 'AZEXO'),
                    $template_slug . '_header' => $template_name . ' ' . __('header', 'AZEXO'),
                    $template_slug . '_data' => $template_name . ' ' . __('data', 'AZEXO'),
                    $template_slug . '_footer' => $template_name . ' ' . __('footer', 'AZEXO'),
                );
                $post_fields = array();
                foreach ($places as $id => $name) {
                    $post_fields[] = array(
                        'id' => $id,
                        'type' => 'select',
                        'multi' => true,
                        'sortable' => true,
                        'title' => $name,
                        'options' => $azexo_fields
                    );
                }

                $this->sections[] = array(
                    'icon' => 'el-icon-cogs',
                    'title' => $template_name,
                    'subsection' => true,
                    'fields' => array_merge(array(
                        array(
                            'id' => $template_slug . '_show_thumbnail',
                            'type' => 'checkbox',
                            'title' => __('Show thumbnail', 'AZEXO'),
                            'default' => '1'
                        ),
                        array(
                            'id' => $template_slug . '_image_thumbnail',
                            'type' => 'checkbox',
                            'title' => __('Only image thumbnail', 'AZEXO'),
                            'default' => '0'
                        ),
                        array(
                            'id' => $template_slug . '_zoom',
                            'type' => 'checkbox',
                            'title' => __('Zoom image on mouse hover', 'AZEXO'),
                            'default' => '0'
                        ),
                        array(
                            'id' => $template_slug . '_gallery_slider_thumbnails',
                            'type' => 'checkbox',
                            'title' => __('Show gallery slider thumbnails', 'AZEXO'),
                            'default' => '0'
                        ),
                        array(
                            'id' => $template_slug . '_show_title',
                            'type' => 'checkbox',
                            'title' => __('Show title', 'AZEXO'),
                            'default' => '1'
                        ),
                        array(
                            'id' => $template_slug . '_show_content',
                            'type' => 'select',
                            'title' => __('Show content/excerpt', 'AZEXO'),
                            'options' => array(
                                'hidden' => __('Hidden', 'AZEXO'),
                                'content' => __('Show content', 'AZEXO'),
                                'excerpt' => __('Show excerpt', 'AZEXO'),
                            ),
                            'default' => 'content',
                        ),
                        array(
                            'id' => $template_slug . '_more_inside_content',
                            'type' => 'checkbox',
                            'title' => __('Show more link inside content', 'AZEXO'),
                            'default' => '1'
                        ),
                        array(
                            'id' => $template_slug . '_share_prefix',
                            'type' => 'text',
                            'title' => __('Share prefix', 'AZEXO'),
                            'default' => '',
                        ),
                        array(
                            'id' => $template_slug . '_share',
                            'type' => 'select',
                            'title' => __('Share place', 'AZEXO'),
                            'options' => array(
                                'hidden' => __('Hidden', 'AZEXO'),
                                'data' => __('Inside post data', 'AZEXO'),
                                'thumbnail' => __('Inside post thumbnail', 'AZEXO'),
                            ),
                            'default' => 'data',
                        ),
                        array(
                            'id' => $template_slug . '_thumbnail_size',
                            'type' => 'text',
                            'title' => __('Thumbnail size', 'AZEXO'),
                            'default' => 'large',
                        ),
                            ), $post_fields)
                );
            }

            $this->sections[] = array(
                'icon' => 'el-icon-cogs',
                'title' => __('Fields configuration', 'AZEXO'),
                'fields' => array()
            );

            foreach ($azexo_fields as $field_slug => $field_name) {
                $this->sections[] = array(
                    'icon' => 'el-icon-cogs',
                    'title' => $field_name,
                    'subsection' => true,
                    'fields' => array(
                        array(
                            'id' => $field_slug . '_prefix',
                            'type' => 'textarea',
                            'title' => __('Prefix', 'AZEXO'),
                            'default' => '',
                        ),
                        array(
                            'id' => $field_slug . '_suffix',
                            'type' => 'textarea',
                            'title' => __('Suffix', 'AZEXO'),
                            'default' => '',
                        ),
                    ),
                );
            }
            
            $this->sections = apply_filters('azexo_settings_sections', $this->sections);

            $theme_info = '<div class="redux-framework-section-desc">';
            $theme_info .= '<p class="redux-framework-theme-data description theme-uri">' . __('<strong>Theme URL:</strong> ', 'AZEXO') . '<a href="' . esc_url($this->theme->get('ThemeURI')) . '" target="_blank">' . $this->theme->get('ThemeURI') . '</a></p>';
            $theme_info .= '<p class="redux-framework-theme-data description theme-author">' . __('<strong>Author:</strong> ', 'AZEXO') . $this->theme->get('Author') . '</p>';
            $theme_info .= '<p class="redux-framework-theme-data description theme-version">' . __('<strong>Version:</strong> ', 'AZEXO') . $this->theme->get('Version') . '</p>';
            $theme_info .= '<p class="redux-framework-theme-data description theme-description">' . $this->theme->get('Description') . '</p>';
            $tabs = $this->theme->get('Tags');
            if (!empty($tabs)) {
                $theme_info .= '<p class="redux-framework-theme-data description theme-tags">' . __('<strong>Tags:</strong> ', 'AZEXO') . implode(', ', $tabs) . '</p>';
            }
            $theme_info .= '</div>';

            $this->sections[] = array(
                'title' => __('Import / Export', 'AZEXO'),
                'desc' => __('Import and Export your Redux Framework settings from file, text or URL.', 'AZEXO'),
                'icon' => 'el-icon-refresh',
                'fields' => array(
                    array(
                        'id' => 'import-export',
                        'type' => 'import_export',
                        'title' => 'Import Export',
                        'subtitle' => 'Save and restore your Redux options',
                        'full_width' => false,
                    ),
                ),
            );

            $this->sections[] = array(
                'type' => 'divide',
            );

            $this->sections[] = array(
                'icon' => 'el-icon-info-sign',
                'title' => __('Theme Information', 'AZEXO'),
                'fields' => array(
                    array(
                        'id' => 'raw-info',
                        'type' => 'raw',
                        'content' => $item_info,
                    )
                ),
            );
        }

        public function setArguments() {

            $theme = wp_get_theme();

            $this->args = array(
                'opt_name' => AZEXO_THEME_NAME,
                'page_slug' => '_options',
                'page_title' => 'Azexo Options',
                'update_notice' => true,
                'admin_bar' => false,
                'menu_type' => 'menu',
                'menu_title' => 'Azexo Options',
                'allow_sub_menu' => true,
                'page_parent_post_type' => 'your_post_type',
                'customizer' => true,
                'default_mark' => '*',
                'hints' =>
                array(
                    'icon' => 'el-icon-question-sign',
                    'icon_position' => 'right',
                    'icon_color' => 'lightgray',
                    'icon_size' => 'normal',
                    'tip_style' =>
                    array(
                        'color' => 'light',
                    ),
                    'tip_position' =>
                    array(
                        'my' => 'top left',
                        'at' => 'bottom right',
                    ),
                    'tip_effect' =>
                    array(
                        'show' =>
                        array(
                            'duration' => '500',
                            'event' => 'mouseover',
                        ),
                        'hide' =>
                        array(
                            'duration' => '500',
                            'event' => 'mouseleave unfocus',
                        ),
                    ),
                ),
                'output' => true,
                'output_tag' => true,
                'page_icon' => 'icon-themes',
                'page_permissions' => 'manage_options',
                'save_defaults' => true,
                'show_import_export' => true,
                'transient_time' => '3600',
                'network_sites' => true,
            );

            $theme = wp_get_theme();
            $this->args["display_name"] = $theme->get("Name");
            $this->args["display_version"] = $theme->get("Version");
        }

    }

    global $reduxConfig;
    $reduxConfig = new AZEXO_Redux_Framework_config();
}