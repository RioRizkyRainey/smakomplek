<?php
/**
 * Single Product Price, including microdata for SEO
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

global $product;
$display_price = wc_price($product->get_display_price()) . $product->get_price_suffix();
?>
<div class="price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
    <h1><?php echo apply_filters('woocommerce_get_price_html', apply_filters('woocommerce_sale_price_html', $display_price, $product), $product); ?></h1>
    <meta itemprop="price" content="<?php print $product->get_price(); ?>" />
    <meta itemprop="priceCurrency" content="<?php echo get_woocommerce_currency(); ?>" />
    <link itemprop="availability" href="http://schema.org/<?php print $product->is_in_stock() ? 'InStock' : 'OutOfStock'; ?>" />
</div>

