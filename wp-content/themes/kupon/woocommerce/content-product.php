<?php
/**
 * The template for displaying product content within loops.
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.4.0
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

global $product, $post;
if (!$product || !$product->is_visible()) {
    return;
}

$options = get_option(AZEXO_THEME_NAME);
if (!isset($product_template)) {
    if (isset($template_name)) {
        $product_template = $template_name;
    } else {
        $product_template = 'shop_product';
    }
}
if (!isset($azwoo_base_tag))
    $azwoo_base_tag = 'li';
$single = ($product_template == 'single_product');
$more_link_text = sprintf(__('Read more<span class="meta-nav"> &rsaquo;</span>', 'AZEXO'));
$thumbnail_size = isset($options[$product_template . '_thumbnail_size']) && !empty($options[$product_template . '_thumbnail_size']) ? $options[$product_template . '_thumbnail_size'] : 'large';
azexo_add_image_size($thumbnail_size);
$image_thumbnail = isset($options[$product_template . '_image_thumbnail']) ? $options[$product_template . '_image_thumbnail'] : false;

$images_links = azwoo_get_images_links($thumbnail_size);

$size = get_image_sizes($thumbnail_size);
?>
<<?php print $azwoo_base_tag; ?> <?php post_class(array(str_replace('_', '-', $product_template))); ?>>

<div class="entry" itemscope itemtype="<?php echo woocommerce_get_product_schema(); ?>">
    <meta itemprop="url" content="<?php the_permalink(); ?>" />
    <?php
    if (!$single) {
        do_action('woocommerce_before_shop_loop_item');
    }
    ?>
    <?php if (isset($options[$product_template . '_show_thumbnail']) && $options[$product_template . '_show_thumbnail']) : ?>
        <div class="entry-thumbnail">            
            <?php if ((count($images_links) > 1) && !$image_thumbnail) : ?>
                <div id="images-<?php the_ID(); ?>" class="images <?php print (isset($options[$product_template . '_gallery_slider_thumbnails']) && esc_attr($options[$product_template . '_gallery_slider_thumbnails']) ? 'thumbnails' : ''); ?>" data-width="<?php print esc_attr($size['width']); ?>" data-height="<?php print esc_attr($size['height']); ?>">
                    <?php
                    wp_enqueue_style('flexslider');
                    wp_enqueue_script('flexslider');
                    foreach ($images_links as $image_link) {
                        ?>                        
                        <?php if ($thumbnail_size == 'full'): ?>
                            <img class="image" src="<?php print esc_url($image_link); ?>" alt="">
                        <?php else: ?>
                            <div class="image <?php print (isset($options[$product_template . '_zoom']) && esc_attr($options[$product_template . '_zoom']) ? 'zoom' : ''); ?>" style="background-image: url(<?php print esc_url($image_link); ?>); height: <?php print esc_attr($size['height']); ?>px;">
                            </div>
                        <?php endif; ?>
                        <?php
                    }
                    ?>
                </div>
            <?php else: ?>  
                <a href="<?php the_permalink(); ?>">
                    <?php
                    $url = azexo_get_the_post_thumbnail(get_the_ID(), $thumbnail_size, true);
                    ?>               
                    <?php if ($thumbnail_size == 'full'): ?>
                        <img class="image" src="<?php print esc_url($url[0]); ?>" alt="">
                    <?php else: ?>
                        <div class="image <?php print (isset($options[$product_template . '_zoom']) && esc_attr($options[$product_template . '_zoom']) ? 'zoom' : ''); ?>" style="background-image: url(<?php print esc_url($url[0]); ?>); height: <?php print esc_attr($size['height']); ?>px;" data-width="<?php print esc_attr($size['width']); ?>" data-height="<?php print esc_attr($size['height']); ?>">
                        </div>
                    <?php endif; ?>
                </a>        
            <?php endif; ?>
            <?php print azexo_entry_meta($product_template, 'thumbnail'); ?>
            <?php if (isset($options[$product_template . '_share']) && ($options[$product_template . '_share'] == 'thumbnail')): ?>
                <div class="entry-share">
                    <?php azexo_entry_share(); ?>
                </div>
            <?php endif; ?>
        </div>
    <?php endif; ?>   
    <?php
    if ($single) {
        do_action('woocommerce_before_single_product_summary');
    }
    ?>
    <div class="entry-data">
        <div class="entry-header">
            <div class="entry-extra">
                <?php print azexo_entry_meta($product_template, 'extra'); ?>
                <?php
                if (!$single) {
                    do_action('woocommerce_before_shop_loop_item_title');
                }
                ?>
            </div>
            <?php
            if (isset($options[$product_template . '_show_title']) && $options[$product_template . '_show_title']) {
                if ($single) {
                    woocommerce_template_single_title();
                } else {
                    ?>
                    <a class="entry-title" href="<?php the_permalink(); ?>">
                        <h3><?php the_title(); ?></h3>
                    </a>                    
                    <?php
                }
            }
            ?>
            <div class="entry-meta">
                <?php
                if (!$single) {
                    do_action('woocommerce_after_shop_loop_item_title');
                }
                ?>
                <?php print azexo_entry_meta($product_template, 'meta'); ?>
            </div>
            <?php print azexo_entry_meta($product_template, 'header'); ?>
        </div>
        <?php if (isset($options[$product_template . '_show_content']) && $options[$product_template . '_show_content'] != 'hidden'): ?>
            <?php if ($options[$product_template . '_show_content'] == 'excerpt') : ?>
                <div class="entry-summary">
                    <?php
                    if ($single) {
                        woocommerce_template_single_excerpt();
                    } else {
                        echo apply_filters('woocommerce_short_description', $post->post_excerpt);
                    }
                    ?>
                </div>
            <?php else : ?>
                <div class="entry-content">
                    <?php
                    if (isset($options[$product_template . '_more_inside_content']) && $options[$product_template . '_more_inside_content'])
                        the_content($more_link_text);
                    else
                        the_content('');
                    ?>                    
                </div>
            <?php endif; ?>  
        <?php endif; ?>   
        <div class="entry-footer">
            <?php print azexo_entry_meta($product_template, 'footer'); ?>
        </div>
        <?php print azexo_entry_meta($product_template, 'data'); ?>
        <?php if (isset($options[$product_template . '_share']) && ($options[$product_template . '_share'] == 'data')): ?>
            <div class="entry-share">
                <div class="helper">
                    <?php print (isset($options[$product_template . '_share_prefix']) ? esc_html($options[$product_template . '_share_prefix']) : ''); ?>
                </div>
                <?php azexo_entry_share(); ?>
            </div>
        <?php endif; ?>
        <?php
        if ($single) {
            do_action('woocommerce_single_product_summary');
        }
        ?>
        <?php
        if (!$single) {
            do_action('woocommerce_after_shop_loop_item');
        }
        ?>
    </div>
</div>
</<?php print $azwoo_base_tag; ?>>
