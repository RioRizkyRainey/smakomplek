<?php
/*
  Field Name: Product stock quantity
 */
?>
<?php
global $product;
$availability = $product->get_availability();
$availability_html = empty($availability['availability']) ? '' : '<p class="stock ' . esc_attr($availability['class']) . '">' . esc_html($availability['availability']) . '</p>';
print apply_filters('woocommerce_stock_html', $availability_html, $availability['availability'], $product);
?>
