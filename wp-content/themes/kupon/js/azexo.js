(function($) {
    "use strict";

    $('#page').css('visibility', 'hidden');
    $("#status").css('display', 'block');
    $("#preloader").css('display', 'block');
    $(function() {
        $('#page').css('visibility', 'visible');
        $("#status").fadeOut("slow");
        $("#preloader").fadeOut("slow");
    });

    $(window).resize(function() {
        initAzexoPostMasonry();
    });

    $.fn.equalizeHeights = function() {
        var max = Math.max.apply(this, $(this).map(function(i, e) {
            return $(e).height()
        }).get());
        if (max > 0)
            this.height(max);
        return max;
    }
    $.fn.equalizeWidths = function() {
        var max = Math.max.apply(this, $(this).map(function(i, e) {
            return $(e).width()
        }).get());
        if (max > 0)
            this.width(max);
        return max;
    }

    jQuery.fn.jRMenuMore = function() {
        $(this).each(function() {
            function alignMenu(obj) {
                var w = 0;
                var mw = $(obj).width() - 30;
                var i = -1;
                var menuhtml = '';
                $.each($(obj).children(), function() {
                    i++;
                    w += $(this).outerWidth(true);
                    if (mw < w) {
                        menuhtml += $('<div>').append($(this).clone()).html();
                        $(this).remove();
                    }
                });
                $(obj).append(
                        '<li  style="position:relative;" class="hideshow">' +
                        '<a href="javascript:void(0)">' +
                        '<i class="fa fa-angle-double-down"></i>' +
                        '</a><ul>' +
                        menuhtml +
                        '</ul></li>'
                        );
                $(obj).children("li.hideshow ul").css("top",
                        $(obj).children("li.hideshow").outerHeight(true) + "px");
                $(obj).children("li.hideshow").click(function() {
                    $(this).children("ul").toggle();
                });
            }
            if ($(this).width() >= $(this).parent().width()) {
                alignMenu(this);
                var robj = this;
                $(window).resize(function() {
                    var cobj = $($($(robj).children("li.hideshow")).children("ul")).html();
                    $(robj).append(cobj);
                    $(robj).children("li.hideshow").remove();
                    alignMenu(robj);
                });
                $(this).addClass("horizontal-responsive-menu");
            }
            else {
                $(this).addClass("horizontal-responsive-menu");
            }
        });
    };
    function initEntryGallery() {
        if ('flexslider' in $.fn) {
            $('.entry .images:not(.thumbnails)').each(function() {
                var gallery = this;
                if ($(gallery).data('flexslider') == undefined) {
                    if ($(gallery).find('.image').length > 1) {
                        $(gallery).flexslider({
                            selector: '.image',
                            prevText: '',
                            nextText: '',
                            touch: true,
                            pauseOnHover: true,
                            mousewheel: false,
                            controlNav: false
                        }).show();
                    }
                }
            });
            $('.entry .images.thumbnails').each(function() {
                function unique_id() {
                    return Math.round(new Date().getTime() + (Math.random() * 100));
                }
                var gallery = this;
                if ($(gallery).data('flexslider') == undefined) {
                    if ($(gallery).find('.image').length > 1) {
                        $(gallery).attr('id', unique_id());
                        var thumbnails = $('<div id="' + $(gallery).attr('id') + '-thumbnails" class="thumbnails"></div>').append('<ul class="slides"></ul>').insertAfter(gallery);
                        $(gallery).find('.image').each(function() {
                            $(this).clone().removeClass('zoom').appendTo($('<li></li>').appendTo($(thumbnails).find('.slides')));
                        });
                        var itemWidth = parseInt($(thumbnails).find('ul.slides li').css('width'), 10);
                        if (!itemWidth)
                            itemWidth = 150;
                        var itemHeight = parseInt($(thumbnails).find('ul.slides li').css('height'), 10);
                        if (!itemHeight)
                            itemHeight = 150;
                        $(thumbnails).flexslider({
                            prevText: '',
                            nextText: '',
                            animation: "slide",
                            controlNav: false,
                            animationLoop: false,
                            pauseOnHover: true,
                            slideshow: false,
                            itemWidth: itemWidth,
                            itemHeight: itemHeight,
                            touch: true,
                            mousewheel: false,
                            asNavFor: '#' + $(gallery).attr('id')
                        });

                        $(gallery).flexslider({
                            selector: '.image',
                            prevText: '',
                            nextText: '',
                            touch: true,
                            pauseOnHover: true,
                            mousewheel: false,
                            controlNav: false,
                            sync: '#' + $(gallery).attr('id') + '-thumbnails'
                        }).show();
                    }
                }
            });
        }
    }
    function initPopup() {
        if ('magnificPopup' in $.fn) {
            $('a.image-popup').magnificPopup({
                type: 'image',
                removalDelay: 300,
                mainClass: 'mfp-fade',
                overflowY: 'scroll'
            });
            $('a.iframe-popup').magnificPopup({
                type: 'iframe',
                removalDelay: 300,
                mainClass: 'mfp-fade',
                overflowY: 'scroll',
                iframe: {
                    markup: '<div class="mfp-iframe-scaler">' +
                            '<div class="mfp-close"></div>' +
                            '<iframe class="mfp-iframe" frameborder="0" allowfullscreen></iframe>' +
                            '</div>',
                    patterns: {
                        youtube: {
                            index: 'youtube.com/',
                            id: 'v=',
                            src: '//www.youtube.com/embed/%id%?autoplay=1'
                        },
                        vimeo: {
                            index: 'vimeo.com/',
                            id: '/',
                            src: '//player.vimeo.com/video/%id%?autoplay=1'
                        },
                        gmaps: {
                            index: '//maps.google.',
                            src: '%id%&output=embed'
                        }
                    },
                    srcAction: 'iframe_src'
                }
            });
        }
    }
    function initAzexoPostList() {
        if ('owlCarousel' in $.fn) {
            $('.owl-carousel.posts-list').each(function() {
                function show(carousel) {
                    if ($(carousel).data('owlCarousel') == undefined) {
                        $(carousel).show();
                        if (width == '')
                            width = $(carousel).width();
                        var items = Math.round($(carousel).width() / width);
                        if (items == 0)
                            items = 1;
                        if (items > 1)
                            items = Math.ceil($(carousel).width() / width);
                        if (full_width) {
                            items = 1;
                        }
                        var options = {
                            items: items,
                            center: ($(carousel).data('center') == 'yes'),
                            loop: ($(carousel).find('.item').length > 1),
                            autoplay: true,
                            autoplayHoverPause: true,
                            nav: true,
                            dots: true,
                            navText: ['', '']
                        };
                        if (margin > 0) {
                            options.margin = margin;
                        }
                        $(carousel).owlCarousel(options).on('translated.owl.carousel', function(event) {
                            try {
                                BackgroundCheck.refresh($(carousel).find('.owl-controls .owl-prev, .owl-controls .owl-next'));
                            } catch (e) {
                            }
                        });
                        $(carousel).find('.item').equalizeHeights();
                        try {
                            BackgroundCheck.init({
                                targets: $(carousel).find('.owl-controls .owl-prev, .owl-controls .owl-next'),
                                images: $(carousel).find('.item .image')
                            });
                        } catch (e) {
                        }
                    }
                }
                function is_visible(carousel) {
                    var visible = true;
                    $(carousel).parents().each(function() {
                        var parent = this;
                        if ($(parent).css('display') == 'none' && visible) {
                            visible = false;
                        }
                    });
                    return visible;
                }
                var carousel = this;
                var width = $(carousel).data('width');
                var height = $(carousel).data('height');
                var margin = isNaN(parseInt($(carousel).data('margin'), 10)) ? 0 : parseInt($(carousel).data('margin'), 10);
                var full_width = $(carousel).data('full-width') == 'yes';
                if (typeof width !== typeof undefined && width !== false && typeof height !== typeof undefined && height !== false) {
                    if (height != '') {
                        $(carousel).find('.item .image').each(function() {
                            $(this).height(height);
                        });
                    }
                    if (is_visible(carousel)) {
                        show(carousel);
                    } else {
                        $(document).on('click.azexo', function() {
                            if ($(carousel).data('owlCarousel') == undefined) {
                                if (is_visible(carousel)) {
                                    show(carousel);
                                }
                            }
                        });
                    }
                }
            });
        }
    }
    function initAzexoPostMasonry() {
        if ('masonry' in $.fn) {
            $('.site-content.masonry-post').each(function() {
                var grid = this;
                var width = $(grid).find('.entry .entry-thumbnail .image[data-width]').attr('data-width');
                var height = $(grid).find('.entry .entry-thumbnail .image[data-height]').attr('data-height');
                var columns = Math.ceil($(grid).width() / width);
                var columnWidth = $(grid).width() / columns;
                $(grid).find('.entry').css('width', columnWidth + 'px');
                var ratio = columnWidth / width;
                $(grid).find('.entry .entry-thumbnail .image').css('height', height * ratio + 'px');
                $(grid).masonry({
                    columnWidth: columnWidth,
                    itemSelector: '.entry'
                });
            });
        }
    }
    function initAzexoCarousel() {
        if ('owlCarousel' in $.fn) {
            $('.carousel').each(function() {
                var carousel = this;
                if ($(carousel).data('owlCarousel') == undefined) {

                    while ($(carousel).find('> .entry').length) {
                        $(carousel).find('> .entry').slice(0, $(carousel).data('contents-per-item')).wrapAll('<div class="item" />');
                    }
                    var r = $(carousel).data('responsive');
                    $(carousel).show();
                    $(carousel).owlCarousel({
                        responsive: window[r],
                        center: ($(carousel).data('center') == 'yes'),
                        margin: $(carousel).data('margin'),
                        autoplay: true,
                        autoplayHoverPause: true,
                        nav: true,
                        navText: ['', '']
                    }).on('translated.owl.carousel', function(event) {
                        try {
                            BackgroundCheck.refresh($(carousel).find('.owl-controls .owl-prev, .owl-controls .owl-next'));
                        } catch (e) {
                        }
                    });
                    try {
                        BackgroundCheck.init({
                            targets: $(carousel).find('.owl-controls .owl-prev, .owl-controls .owl-next'),
                            images: $(carousel).find('.item .image')
                        });
                    } catch (e) {
                    }
                }
            });
        }
    }
    function initMobileMenu() {
        $(".mobile-menu-button span").on("tap click", function(e) {
            e.preventDefault();
            if ($(".mobile-menu > div > ul").is(":visible")) {
                $(".mobile-menu > div > ul").slideUp(200)
            } else {
                $(".mobile-menu > div > ul").slideDown(200)
            }
        });

        $('.mobile-menu ul.nav-menu:not(.vc) > li.menu-item.menu-item-has-children, .mobile-menu ul.sub-menu:not(.vc) > li.menu-item.menu-item-has-children').append('<span class="mobile-arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>');
        $('.mobile-menu ul.nav-menu:not(.vc) > .mega, .mobile-menu ul.sub-menu:not(.vc) > .mega').append('<span class="mobile-arrow"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></span>');

        $(".mobile-menu > div > ul > li.menu-item-has-children > span.mobile-arrow").on("tap click", function(e) {
            e.preventDefault();
            if ($(this).closest("li.menu-item-has-children").find("> ul.sub-menu").is(":visible")) {
                $(this).closest("li.menu-item-has-children").find("> ul.sub-menu").slideUp(200);
                $(this).closest("li.menu-item-has-children").removeClass("open-sub")
            } else {
                $(this).closest("li.menu-item-has-children").addClass("open-sub");
                $(this).closest("li.menu-item-has-children").find("> ul.sub-menu").slideDown(200)
            }
        });
        $(".mobile-menu > div > ul > li.mega > span.mobile-arrow").on("tap click", function(e) {
            e.preventDefault();
            if ($(this).closest("li.mega").find("> .page").is(":visible")) {
                $(this).closest("li.mega").find("> .page").slideUp(200);
                $(this).closest("li.mega").removeClass("open-sub")
            } else {
                $(this).closest("li.mega").addClass("open-sub");
                $(this).closest("li.mega").find("> .page").slideDown(200)
            }
        });
        $(".mobile-menu > div > ul > li.menu-item-has-children > ul.sub-menu > li.menu-item-has-children > span.mobile-arrow").on("tap click", function(e) {
            e.preventDefault();
            if ($(this).parent().find("ul.sub-menu").is(":visible")) {
                $(this).parent().find("ul.sub-menu").slideUp(200);
                $(this).parent().removeClass("open-sub")
            } else {
                $(this).parent().addClass("open-sub");
                $(this).parent().find("ul.sub-menu").slideDown(200)
            }
        });
        $(".mobile-menu ul li > a").on("click", function() {
            if ($(this).attr("href") !== "http://#" && $(this).attr("href") !== "#") {
            }
        })
    }
    function initSearchForm() {
        $('.searchform input[name="s"]').attr('placeholder', $('.searchform [type="submit"]').val()).on('keydown', function(event) {
            if (event.keyCode == 13) {
                $('.searchform [type="submit"]').click();
            }
        });
        $('.search-wrapper i').click(function() {
            $(this).parent().find('.searchform').show().find('input[name="s"]').focus();
            $(this).hide();
        });
        $('.search-wrapper input[name="s"]').blur(function() {
            $(this).closest('.search-wrapper').find('i').show();
            $(this).closest('.searchform').hide();
        });
    }
    function initMegaMenu() {
        $('.primary-navigation .nav-menu, .secondary-navigation .nav-menu').each(function() {
            var menu = this;
            $(menu).find('.mega:not(.compact) .page').each(function() {
                var page = this;
                function on_hover() {
                    $(page).css('width', $(menu).closest('.container').width() + 'px');
                    $(page).css('left', ($(menu).closest('.container').offset().left - $(this).offset().left) + 'px');
                }
                $(page).parent().hover(on_hover);
                $(window).load(function() {
                    on_hover.call($(page).parent());
                });
                $(window).resize(function() {
                    on_hover.call($(page).parent());
                });
            });
        });
        $('.widget_nav_menu .menu').each(function() {
            var menu = this;
            $(menu).find('.page').each(function() {
                var page = this;
                function on_hover() {
                    $(page).css('width', $(menu).closest('.container').width() - $(this).outerWidth() + 'px');
                }
                $(page).parent().hover(on_hover);
                $(window).load(function() {
                    on_hover.call($(page).parent());
                });
                $(window).resize(function() {
                    on_hover.call($(page).parent());
                });
            });
        });
    }
    function initStickyMenu() {
        var header_main_top = 0;
        header_main_top = $('.header-main').offset().top;
        $('.site-header').imagesLoaded(function() {
            var interval = setInterval(function() {
                if (!$('.site-header').hasClass('scrolled')) {
                    header_main_top = $('.header-main').offset().top;
                    clearInterval(interval);
                }
            }, 100);
            $(window).scroll(function() {
                if ($(window).scrollTop() > header_main_top) {
                    $('.site-header').addClass('scrolled');
                    if ($('nav.mobile-menu').css('display') == 'none')
                        $('.site-header .header-main').addClass('animated fadeInDown');
                } else {
                    $('.site-header').removeClass('scrolled');
                    if ($('nav.mobile-menu').css('display') == 'none')
                        $('.site-header .header-main').removeClass('animated fadeInDown');
                }
            });
        });
    }
    function initStickySidebar() {
        if ('stick_in_parent' in $.fn) {
            if ($(window).width() > 1100)
                $("#tertiary .sidebar-inner").stick_in_parent({
                    offset_top: $('.header-main').height()
                });
        }
    }
    function initImageZoom() {
        $('.image.zoom').each(function() {
            var image = this;
            var img = new Image();
            img.src = $(image).css('background-image').replace(/url\(|\)$|"|'/ig, '');
            $(img).imagesLoaded(function() {
                if (img.width > $(image).width() || img.height > $(image).height()) {
                    $(image).off('mouseenter.azexo').on('mouseenter.azexo', function() {
                        $(image).css('background-size', 'initial');
                    });
                    $(image).off('mouseleave.azexo').on('mouseleave.azexo', function() {
                        $(image).css('background-size', '');
                        $(image).css('background-position', '');
                    });
                    $(image).off('mousemove.azexo').on('mousemove.azexo', function(event) {
                        $(image).css('background-position', event.offsetX / $(image).width() * 100 + '% ' + event.offsetY / $(image).height() * 100 + '%');
                    });
                }
            });
        });
    }
    function initLinkScrolling() {
        $('a[href*="#"].roll, .roll a[href*="#"]').on('click', function(e) {
            if (this.href.split('#')[0] == '' || window.location.href.split('#')[0] == this.href.split('#')[0]) {
                e.preventDefault();
                var hash = this.hash;
                $('html, body').stop().animate({
                    'scrollTop': $(hash).offset().top - $('.header-main').height()
                }, 2000)
            }
        });
    }
    $(function() {
        initMobileMenu();
        initMegaMenu();
        initSearchForm();
        initStickySidebar();
        initEntryGallery();
        initStickyMenu();
        initAzexoPostList();
        initAzexoPostMasonry();
        initAzexoCarousel();
        initImageZoom();
        initLinkScrolling();
        initPopup();
        if (document.documentElement.clientWidth > 768) {
            if (typeof scrollReveal === 'function') {
                window.scrollReveal = new scrollReveal();
            }
        }
        $(document).ajaxComplete(function() {
            initEntryGallery();
            initAzexoPostList();
            initAzexoPostMasonry();
            initImageZoom();
        });
    });
})(jQuery);