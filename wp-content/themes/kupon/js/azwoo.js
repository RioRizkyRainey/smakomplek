(function($) {
    "use strict";

    function initWCFS() {
        if ($('.cmb2-id-product-gallery-1').length) {
            $(".cmb2-id-product-gallery-1").hide();
            $('.cmb2-id-product-image input').on('change', function() {
                if ($(this).value !== "") {
                    $(".cmb2-id-product-gallery-1").show();
                } else {
                    $(".cmb2-id-product-gallery-1").hide();
                }
            });
        }
        if ($('.cmb2-id-product-gallery-2').length) {
            $(".cmb2-id-product-gallery-2").hide();
            $('.cmb2-id-product-gallery-1 input').on('change', function() {
                if ($(this).value !== "") {
                    $(".cmb2-id-product-gallery-2").show();
                } else {
                    $(".cmb2-id-product-gallery-2").hide();
                }
            });
        }
        if ($('.cmb2-id-product-gallery-3').length) {
            $(".cmb2-id-product-gallery-3").hide();
            $('.cmb2-id-product-gallery-2 input').on('change', function() {
                if ($(this).value !== "") {
                    $(".cmb2-id-product-gallery-3").show();
                } else {
                    $(".cmb2-id-product-gallery-3").hide();
                }
            });
        }
        if ($('.cmb2-id-product-gallery-4').length) {
            $(".cmb2-id-product-gallery-4").hide();
            $('.cmb2-id-product-gallery-3 input').on('change', function() {
                if ($(this).value !== "") {
                    $(".cmb2-id-product-gallery-4").show();
                } else {
                    $(".cmb2-id-product-gallery-4").hide();
                }
            });
        }
        if ($('.cmb2-id-product-gallery-5').length) {
            $(".cmb2-id-product-gallery-5").hide();
            $('.cmb2-id-product-gallery-4 input').on('change', function() {
                if ($(this).value !== "") {
                    $(".cmb2-id-product-gallery-5").show();
                } else {
                    $(".cmb2-id-product-gallery-5").hide();
                }
            });
        }
        if ($('.cmb2-id-product-gallery-6').length) {
            $(".cmb2-id-product-gallery-6").hide();
            $('.cmb2-id-product-gallery-5 input').on('change', function() {
                if ($(this).value !== "") {
                    $(".cmb2-id-product-gallery-6").show();
                } else {
                    $(".cmb2-id-product-gallery-6").hide();
                }
            });
        }
        if ($('.cmb2-id-product-virtual').length) {
            $(".cmb2-id-product-virtual input[type='checkbox']").on('change', function() {
                if ($(this).is(':checked')) {
                    $(".cmb2-id-product-weight, .cmb2-id-product-length, .cmb2-id-product-width, .cmb2-id-product-height").hide();
                }
                else {
                    $(".cmb2-id-product-weight, .cmb2-id-product-length, .cmb2-id-product-width, .cmb2-id-product-height").show();
                }
            });
        }
        if ($('.cmb2-id-product-downloadable').length) {
            $(".cmb2-id-product-file, .cmb2-id-product-file-name").hide();
            $(".cmb2-id-product-downloadable input[type='checkbox']").on('change', function() {
                if ($(this).is(':checked')) {
                    $(".cmb2-id-product-file, .cmb2-id-product-file-name").show();
                }
                else {
                    $(".cmb2-id-product-file, .cmb2-id-product-file-name").hide();
                }
            });
        }

    }
    function initProducts() {
        if ('select2' in $.fn) {
            $('select[name="product_cat"]').select2();
            $('select[name="product_category"]').select2();
        }
    }
    function initProductCategoriesWidget() {
        $('ul.product-categories li.cat-parent a').click(function(event) {
            event.stopPropagation();
        });
        $('ul.product-categories li.cat-parent').click(function() {
            var item = this;
            var children = $(this).find('> ul.children');
            if (children.css('display') == 'none') {
                children.stop(true, true).slideDown();
                children.show();
                $(item).find('> a').addClass('open');
            } else {
                children.stop(true, true).slideUp(400, function() {
                    children.hide();
                    $(item).find('> a').removeClass('open');
                });
            }
        });
    }
    function initQuantity() {
        $('.quantity input[name="quantity"]').each(function() {
            var qty_el = this;
            $(qty_el).parent().find('.qty-increase').off('click.azwoo').on('click.azwoo', function() {
                var qty = qty_el.value;
                if (!isNaN(qty))
                    qty_el.value++;
            });
            $(qty_el).parent().find('.qty-decrease').off('click.azwoo').on('click.azwoo', function() {
                var qty = qty_el.value;
                if (!isNaN(qty) && qty > 1)
                    qty_el.value--;
            });
        });
        return false;
    }
    function initCountDown() {
        if ('countdown' in $.fn) {
            $('.time-left .time').each(function() {
                var timer = this;
                if ($(timer).data('countdownInstance') == undefined) {
                    $(timer).countdown($(timer).data('time'), function(event) {
                        $(timer).find('.days .count').text(event.offset.totalDays);
                        $(timer).find('.hours .count').text(event.offset.hours);
                        $(timer).find('.minutes .count').text(event.offset.minutes);
                        $(timer).find('.seconds .count').text(event.offset.seconds);
                    });
                }
            });
        }
    }
    $(function() {
        initWCFS();
        initProducts();
        initProductCategoriesWidget();
        initQuantity();
        initCountDown();
        $(document).ajaxComplete(function() {
            initQuantity();
            initCountDown();
        });
        $(document.body).on('adding_to_cart', function(event, button, data) {
        });
    });
})(jQuery);