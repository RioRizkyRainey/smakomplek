<?php
if (post_password_required())
    return;
?>

<div id="comments" class="comments-area">
    <?php if (have_comments()) : ?>
        <h3 class="comments-title">
            <?php comments_number(__('No Comments', 'AZEXO'), __('One Comment', 'AZEXO'), __('% Comments', 'AZEXO'));
            ?>
        </h3>

        <ol class="comment-list">
            <?php
            $options = get_option(AZEXO_THEME_NAME);
            wp_list_comments(array(
                'walker' => new Azexo_Walker_Comment(),
                'avatar_size' => isset($options['avatar_size']) ? $options['avatar_size'] : 32,
            ));
            ?>
        </ol><!-- .comment-list -->

        <?php
        // Are there comments to navigate through?
        if (get_comment_pages_count() > 1 && get_option('page_comments')) :
            ?>
            <nav class="navigation comment-navigation" role="navigation">
                <h1 class="screen-reader-text section-heading"><?php _e('Comment navigation', 'AZEXO'); ?></h1>
                <div class="nav-previous"><?php previous_comments_link(__('&larr; Older Comments', 'AZEXO')); ?></div>
                <div class="nav-next"><?php next_comments_link(__('Newer Comments &rarr;', 'AZEXO')); ?></div>
            </nav><!-- .comment-navigation -->
        <?php endif; // Check for comment navigation   ?>

        <?php if (!comments_open() && get_comments_number()) : ?>
            <p class="no-comments"><?php _e('Comments are closed.', 'AZEXO'); ?></p>
        <?php endif; ?>

    <?php endif; // have_comments()   ?>

    <?php
    if (comments_open()) :
        $args = array(
            'id_form' => 'commentform',
            'id_submit' => 'submit',
            'submit_field' => '<div class="form-submit">%1$s %2$s</div>',
            'title_reply' => __('Leave a Reply', 'AZEXO'),
            'title_reply_to' => __('Leave a Reply to %s', 'AZEXO'),
            'cancel_reply_link' => __('Cancel Reply', 'AZEXO'),
            'label_submit' => __('submit', 'AZEXO'),
            'comment_field' => '<textarea id="comment" name="comment" cols="45" rows="8" aria-required="true" placeholder="' . __('message', 'AZEXO') . '"></textarea>',
            'must_log_in' => '<p class="must-log-in">' .
            sprintf(
                    __('You must be <a href="%s">logged in</a> to post a comment.', 'AZEXO'), esc_url(wp_login_url(apply_filters('the_permalink', get_permalink())))
            ) . '</p>',
            'logged_in_as' => '<p class="logged-in-as">' .
            sprintf(
                    __('Logged in as <a href="%1$s">%2$s</a>. <a href="%3$s" title="Log out of this account">Log out?</a>', 'AZEXO'), esc_url(admin_url('profile.php')), $user_identity, esc_url(wp_logout_url(apply_filters('the_permalink', get_permalink())))
            ) . '</p>',
            'comment_notes_before' => '',
            'comment_notes_after' => '',
            'fields' => apply_filters('comment_form_default_fields', array(
                'author' =>
                '<div class="comment-fields"><div class="author-email"><input id="author" name="author" type="text" value="' . esc_attr($commenter['comment_author']) . '" size="30" placeholder="' . __('name', 'AZEXO') . '"/>',
                'email' =>
                '<input id="email" name="email" type="text" value="' . esc_attr($commenter['comment_author_email']) . '" size="30" placeholder="' . __('email', 'AZEXO') . '" /></div>',
                'url' =>
                '<input id="url" name="url" type="text" value="' . esc_attr($commenter['comment_author_url']) . '" size="30" placeholder="' . __('website', 'AZEXO') . '" /></div>'
                    )
            ),
        );
        comment_form($args);
    endif;
    ?>
</div><!-- #comments -->