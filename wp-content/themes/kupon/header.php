<?php
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
    <!--<![endif]-->
    <head>
        <?php $options = get_option(AZEXO_THEME_NAME); ?>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta name="viewport" content="width=device-width">
        <!--Shortcut icon-->
        <?php
        if (isset($options['favicon']['url']) && !empty($options['favicon']['url'])) {
            ?>
            <link rel="shortcut icon" href="<?php echo esc_url($options['favicon']['url']); ?>" />
        <?php } ?>
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="pingback" href="<?php esc_url(bloginfo('pingback_url')); ?>">        
        <?php
        if (function_exists('wp_site_icon')) {
            wp_site_icon();
        }
        ?>
        <?php wp_head(); ?>
    </head>

    <body <?php body_class(); ?>>        
        <div id="preloader"><div id="status"></div></div>
        <div id="page" class="hfeed site">
            <header id="masthead" class="site-header clearfix">
                <?php
                if (!is_page_template('page-templates/fullscreen.php')) {
                    get_sidebar('header');
                }
                ?>                
                <div class="header-main clearfix <?php print (is_page_template('page-templates/fullscreen.php') ? 'fs' : ''); ?>">
                    <div class="container">
                        <?php
                        if (isset($options['header'])) {
                            foreach ((array) $options['header'] as $part) {
                                switch ($part) {
                                    case 'logo':
                                        if (isset($options['logo']['url']) && !empty($options['logo']['url'])) {
                                            ?>
                                            <a class="site-title" href="<?php echo esc_url(home_url('/')); ?>" rel="home"><img width="230px" src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="logo"></a>
                                            <?php
                                        }
                                        break;
                                    case 'search':
                                        azexo_get_search_form();
                                        break;
                                    case 'mobile_menu_button':
                                        ?>
                                        <div class="mobile-menu-button"><span><i class="fa fa-bars"></i></span></div>                    
                                        <?php
                                        break;
                                    case 'mobile_menu':
                                        ?>
                                        <nav class="site-navigation mobile-menu">
                                            <?php
                                            if (has_nav_menu('primary')) {
                                                wp_nav_menu(array(
                                                    'theme_location' => 'primary',
                                                    'menu_class' => 'nav-menu',
                                                    'menu_id' => 'primary-menu-mobile',
                                                    'walker' => new Azexo_Walker_Nav_Menu(),
                                                ));
                                            }
                                            ?>
                                        </nav>
                                        <?php
                                        break;
                                    case 'primary_menu':
                                        ?>
                                        <nav class="site-navigation primary-navigation">
                                            <?php
                                            if (has_nav_menu('primary')) {
                                                wp_nav_menu(array(
                                                    'theme_location' => 'primary',
                                                    'menu_class' => 'nav-menu',
                                                    'menu_id' => 'primary-menu',
                                                    'walker' => new Azexo_Walker_Nav_Menu(),
                                                ));
                                            }
                                            ?>
                                        </nav>
                                        <?php
                                        break;
                                    case 'secondary_menu':
                                        ?><nav class="secondary-navigation"> <?php
                                        if (has_nav_menu('secondary')) {
                                            wp_nav_menu(array(
                                                'theme_location' => 'secondary',
                                                'menu_class' => 'nav-menu',
                                                'menu_id' => 'secondary-menu',
                                                'walker' => new Azexo_Walker_Nav_Menu(),
                                            ));
                                        }
                                        ?></nav> <?php
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                        ?>                        
                    </div>
                </div>
                <?php
                if (!is_page_template('page-templates/fullscreen.php')) {
                    get_sidebar('middle');
                }
                ?>                                
            </header><!-- #masthead -->
            <div id="main" class="site-main">
