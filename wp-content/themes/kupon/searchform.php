<form role="search" method="get" class="searchform" action="<?php print esc_url(home_url('/')); ?>">
    <div class="searchform-wrapper">
        <label class="screen-reader-text"><?php print __('Search for:', 'AZEXO'); ?></label>
        <input type="text" value="<?php print get_search_query(); ?>" name="s" />
        <div class="submit"><input type="submit" value="<?php print esc_attr__('Search', 'AZEXO'); ?>"></div>
    </div>
</form>