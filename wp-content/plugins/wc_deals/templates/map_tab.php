<?php
$protocol = is_ssl() ? 'https' : 'http';
wp_enqueue_style('wcd-frontend', WCD_URL . '/css/frontend.css');
wp_enqueue_script('wcd-frontend', WCD_URL . '/js/frontend.js', false, false, true);
wp_enqueue_script('wcd-googlemap', $protocol . '://maps.googleapis.com/maps/api/js?sensor=false', false, false, true);

if (!empty($deal_markers)):
    $deal_markers = explode("\n", $deal_markers);
    $markers = array();
    foreach ($deal_markers as $deal_marker) {
        $marker = explode(",", $deal_marker);
        $markers[] = array('latitude' => trim($marker[0]), 'longitude' => trim($marker[1]));
    }
    ?>
    <div id="deal-map" data-markers='<?php echo json_encode($markers) ?>'></div>
<?php endif; ?>