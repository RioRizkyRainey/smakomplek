<?php
if (!WCV_Vendors::is_vendor(get_current_user_id()) || !current_user_can('manage_product')) {
    return;
}

$date_range = '';
$unlimited_expire = false;
$deal_id = isset($_GET['deal_id']) ? $_GET['deal_id'] : '';
$item_id = isset($_GET['item_id']) ? $_GET['item_id'] : '';
$action = isset($_GET['action']) ? $_GET['action'] : '';
$subaction = isset($_GET['subaction']) ? $_GET['subaction'] : '';


$deal_title = isset($_POST['deal_title']) ? $_POST['deal_title'] : '';
$deal_description = isset($_POST['deal_description']) ? $_POST['deal_description'] : '';
$deal_featured_image = isset($_POST['deal_featured_image']) ? $_POST['deal_featured_image'] : '';
$deal_cat = isset($_POST['deal_cat']) ? $_POST['deal_cat'] : '';
$deal_new_category = isset($_POST['deal_title']) ? $_POST['deal_new_category'] : '';
$deal_location = isset($_POST['deal_location']) ? $_POST['deal_location'] : '';
$deal_new_location = isset($_POST['deal_new_location']) ? $_POST['deal_new_location'] : '';
$deal_start = isset($_POST['deal_start']) ? strtotime($_POST['deal_start']) : current_time('timestamp');
$deal_expire = isset($_POST['deal_expire']) ? strtotime($_POST['deal_expire']) : '';
$deal_link = isset($_POST['deal_link']) ? $_POST['deal_link'] : '';
$deal_items = isset($_POST['deal_items']) ? $_POST['deal_items'] : '';
$deal_item_vouchers = isset($_POST['deal_item_vouchers']) ? $_POST['deal_item_vouchers'] : '';
$deal_price = isset($_POST['deal_price']) ? $_POST['deal_price'] : '';
$deal_sale_price = isset($_POST['deal_sale_price']) ? $_POST['deal_sale_price'] : '';
$deal_voucher_expire = isset($_POST['deal_voucher_expire']) ? strtotime($_POST['deal_voucher_expire']) : '';
$deal_in_short = isset($_POST['deal_in_short']) ? $_POST['deal_in_short'] : '';
$deal_markers = isset($_POST['deal_markers']) ? $_POST['deal_markers'] : '';
$deal_images = isset($_POST['deal_images']) ? $_POST['deal_images'] : '';

function show_deals() {
    $deals = wcd_get_deals();
    wc_print_notices();
    ?> 
    <div id="deal-toolbar" class="btn-group">
        <a href="<?php echo esc_url(add_query_arg(array('action' => 'add'))); ?>" class="btn btn-default">
            <i class="fa fa-plus"></i> <?php _e('Add new deal', 'wcd') ?>
        </a>
        <?php if (function_exists('woo_vou_plugin_loaded')): ?>
            <a href="<?php echo esc_url(add_query_arg(array('action' => 'woo_vou_check_code'))); ?>" class="btn btn-default">
                <i class="fa fa-check-square-o"></i> <?php _e('Check voucher code', 'wcd') ?>
            </a>
        <?php endif; ?>
    </div>
    <table data-toggle="table" data-search="true" data-classes="table table-striped" data-searchtext="<?php esc_attr_e('Search', 'wcd') ?>">
        <thead>
            <tr>
                <th data-field="deal" data-sortable="true">
                    <?php _e('Deal', 'wcd'); ?>
                </th>
                <th data-field="status" data-sortable="true">
                    <?php _e('Status', 'wcd'); ?>
                </th>
                <th data-field="category" data-sortable="true">
                    <?php _e('Category', 'wcd'); ?>
                </th>
                <th data-field="purchases" data-sortable="true">
                    <?php _e('Purchases', 'wcd'); ?>
                </th>
                <th data-field="action" data-sortable="true">
                    <?php _e('Action', 'wcd'); ?>
                </th>
            </tr>
        </thead>
        <tbody>
            <?php
            if (is_array($deals)) {
                foreach ($deals as $deal) {
                    if (empty($deal->deal_id))
                        continue;
                    $expired = false;
                    if (is_numeric($deal->deal_expire) && $deal->deal_expire <= current_time('timestamp')) {
                        $expired = true;
                    }
                    ?>
                    <tr class="<?php echo $expired ? 'disabled' : '' ?>">
                        <td class="deal-name-td">
                            <a href="<?php echo get_permalink($deal->deal_id) ?>" target="_blank">
                                <?php echo $deal->deal_title; ?>
                            </a>
                        </td>
                        <td>
                            <?php
                            if ($deal->status == 'publish') {
                                if ($expired) {
                                    _e('Expired', 'wcd');
                                } else {
                                    if (is_numeric($deal->stock) && $deal->stock == 0) {
                                        _e('Sold out', 'wcd');
                                    } else {
                                        _e('Live', 'wcd');
                                    }
                                }
                            } else {
                                _e('Pending', 'wcd');
                            }
                            ?>
                        </td>
                        <td>
                            <?php echo $deal->category; ?>
                        </td>
                        <td>
                            <?php echo $deal->purchases; ?>
                        </td>
                        <td>
                            <?php if (false): ?>
                                <a title="<?php esc_attr_e('Edit Deal', 'wcd') ?>" href="<?php echo esc_url(add_query_arg(array('deal_id' => $deal->deal_id, 'action' => 'edit'))); ?>">
                                    <i class="fa fa-pencil"></i>
                                </a>
                            <?php endif; ?>
                            <?php if (!function_exists('woo_vou_plugin_loaded')): ?>
                                <a title="<?php esc_attr_e('View Vouchers', 'wcd') ?>" href="<?php echo esc_url(add_query_arg(array('deal_id' => $deal->deal_id, 'action' => 'vouchers'))); ?>">
                                    <i class="fa fa-cog"></i>
                                </a>
                            <?php endif; ?>
                            <a title="<?php esc_attr_e('Remove Deal', 'wcd') ?>" href="<?php echo esc_url(add_query_arg(array('deal_id' => $deal->deal_id, 'action' => 'remove'))); ?>">
                                <i class="fa fa-remove"></i>
                            </a>
                        </td>
                    </tr>
                    <?php
                }
            }
            ?>
        </tbody>
    </table>
    <?php
}

switch ($action) {
    case 'add':
        $show_form = true;
        if (isset($_POST['wcd-deal-nonce'])) {
            if (wp_verify_nonce($_POST['wcd-deal-nonce'], 'submit-deal')) {
                if (!empty($deal_title)) {
                    if (!empty($deal_description)) {
                        if (!empty($deal_featured_image)) {
                            if (!empty($deal_cat) || !empty($deal_new_category)) {
                                if (!empty($deal_location) || !empty($deal_new_location)) {
                                    if (( ( empty($unlimited_expire) || !$unlimited_expire ) && !empty($deal_expire) ) || $unlimited_expire) {
                                        $check_range = true;
                                        if (!empty($date_range) && !$unlimited_expire) {
                                            if ($deal_expire - $deal_start >= $date_range * 24 * 60 * 60) {
                                                $check_range = false;
                                                wc_add_notice(__('Maximum range between days is ', 'wcd') . ' ' . $date_range, 'error');
                                            }
                                        }
                                        if ($check_range) {
                                            if (empty($deal_expire)) {
                                                $deal_expire = '-1';
                                            }
                                            if (!empty($deal_items) && is_numeric($deal_items)) {
                                                if (!empty($deal_price) && is_numeric($deal_price)) {
                                                    if (!empty($deal_sale_price) && is_numeric($deal_sale_price)) {
                                                        if (!empty($deal_in_short)) {
                                                            $deal_id = wcd_add_update_deal($_POST, $deal_id);
                                                            if (isset($_GET['deal_id'])) {
                                                                if ($deal_id) {
                                                                    wc_add_notice(__('Deal updated', 'wcd'), 'success');
                                                                    $show_form = false;
                                                                }
                                                            } else {
                                                                if (is_numeric($deal_id)) {
                                                                    $show_form = false;
                                                                    wc_add_notice(__('New deal submitted', 'wcd'), 'success');
                                                                }
                                                            }
                                                        } else {
                                                            wc_add_notice(__('You need to input short description of the deal', 'wcd'), 'error');
                                                        }
                                                    } else {
                                                        wc_add_notice(__('You need to specify sale price of the deal ( number only with max two decimal separated by . )', 'wcd'), 'error');
                                                    }
                                                } else {
                                                    wc_add_notice(__('You need to specify real price of the deal ( number only with max two decimal separated by . )', 'wcd'), 'error');
                                                }
                                            } else {
                                                wc_add_notice(__('You need to specify number of items you wish to sell', 'wcd'), 'error');
                                            }
                                        }
                                    } else {
                                        wc_add_notice(__('You need to specify expire date', 'wcd'), 'error');
                                    }
                                } else {
                                    wc_add_notice(__('You need to select or submit new location', 'wcd'), 'error');
                                }
                            } else {
                                wc_add_notice(__('You need to select or submit new category', 'wcd'), 'error');
                            }
                        } else {
                            wc_add_notice(__('You need to select featuerd image for the deal', 'wcd'), 'error');
                        }
                    } else {
                        wc_add_notice(__('Description is required', 'wcd'), 'error');
                    }
                } else {
                    wc_add_notice(__('Title is required', 'wcd'), 'error');
                }
            }
        }
        if ($show_form) {
            wp_enqueue_script('jquery-ui-tabs');
            wp_enqueue_style('datetimepicker', WCD_URL . '/css/jquery.datetimepicker.css');
            wp_enqueue_script('datetimepicker', WCD_URL . '/js/jquery.datetimepicker.js', false, false, true);

            wp_enqueue_style('wcd-frontend', WCD_URL . '/css/frontend.css');
            wp_enqueue_script('wcd-frontend', WCD_URL . '/js/frontend.js', false, false, true);
            $protocol = is_ssl() ? 'https' : 'http';
            wp_enqueue_script('wcd-googlemap', $protocol . '://maps.googleapis.com/maps/api/js?sensor=false', false, false, true);

            $commission = 0;
            $vendor_commission = WCV_Vendors::get_default_commission(get_current_user_id());
            $default_commission = WC_Vendors::$pv_options->get_option('default_commission');
            if ($vendor_commission != '' && $vendor_commission !== false) {
                $commission = $vendor_commission;
            } else if ($default_commission != '' && $default_commission !== false) {
                $commission = $default_commission;
            }
            wc_add_notice(sprintf(__('For every sale of this deal you will pay commission: %1$d%%', 'wcd'), 100 - $commission), 'notice');

            wc_print_notices();
            ?>	
            <h3><?php _e('Add new deal', 'wcd') ?></h3>
            <form id="deal-form" method="POST">
                <div id="deal-wizard">
                    <ul>
                        <li><a href="#deal-info"><?php _e('Texts and images', 'wcd'); ?></a></li>
                        <li><a href="#deal-parameters"><?php _e('Parameters', 'wcd'); ?></a></li>
                        <li><a href="#deal-variations"><?php _e('Variations', 'wcd'); ?></a></li>
                        <li><a href="#deal-appearance"><?php _e('Appearance on site', 'wcd'); ?></a></li>
                        <li><a href="#deal-misc"><?php _e('Miscellaneous', 'wcd'); ?></a></li>
                    </ul>
                    <div id="deal-info">
                        <div class="input-group">
                            <label for="deal_title"><?php _e('Deal title', 'wcd'); ?> <span class="required">*</span></label>
                            <input type="text" name="deal_title" id="deal_title" value="<?php echo esc_attr($deal_title) ?>" class="form-control" data-validation="required"  data-error="<?php esc_attr_e('Please input deal description', 'wcd'); ?>">
                            <p class="description"><?php _e('Input title for the deal.', 'wcd'); ?></p>
                        </div>
                        <div class="input-group">
                            <label for="deal_description" data-error="<?php esc_attr_e('Please type description of the deal', 'wcd'); ?>"><?php _e('Deal Description', 'wcd'); ?> <span class="required">*</span></label>
                            <?php wp_editor($deal_description, 'deal_description'); ?>
                            <p class="description"><?php _e('Input description of the deal.', 'wcd'); ?></p>
                        </div>
                        <div class="input-group">
                            <label for="deal_in_short"><?php _e('Short description', 'wcd'); ?> <span class="required">*</span></label>
                            <textarea type="text" name="deal_in_short" id="deal_in_short" class="form-control" data-validation="required" data-error="<?php esc_attr_e('Please input deal in short', 'wcd'); ?>"><?php echo $deal_in_short ?></textarea>
                            <p class="description"><?php _e('Input description which will appear in the deal single page sidebar.', 'wcd'); ?></p>
                        </div>
                        <div class="input-group">
                            <label for="deal_featured_image"><?php _e('Deal main image', 'wcd'); ?> <span class="required">*</span></label>
                            <input type="hidden" name="deal_featured_image" id="deal_featured_image" value="<?php esc_attr($deal_featured_image) ?>" data-validation="required"  data-error="<?php esc_attr_e('Please input deal presentation image', 'wcd'); ?>">
                            <div class="upload-image-wrap featured-image-wrap"></div>
                            <a href="javascript:;" class="image-upload featured-image"><?php _e('Select featured image', 'wcd') ?></a>
                            <p class="description"><?php _e('Upload and select featured image for the deal.', 'wcd'); ?></p>
                        </div>			    
                        <div class="input-group">
                            <label for="deal_images"><?php _e('Deal additional images', 'wcd'); ?> </label>
                            <input type="hidden" name="deal_images" id="deal_images" class="form-control">
                            <div class="deal-images-wrap"></div>
                            <a href="javascript:;" class="image-upload deal-images"><?php _e('Select deal images', 'wcd') ?></a>
                            <p class="description"><?php _e('Choose images for the deal. Drag and drop to change their order.', 'wcd'); ?></p>
                        </div>
                    </div>

                    <div id="deal-parameters">
                        <div class="input-group">
                            <label for="deal_start"><?php _e('Deal start date', 'wcd'); ?> </label>
                            <input type="text" name="deal_start" id="deal_start" value="<?php echo date('Y-m-d', $deal_start) ?>" class="form-control" readonly="readonly" data-range="<?php echo esc_attr($date_range); ?>">
                            <p class="description"><?php _e('Set start date for the deal. If this field is empty current time will be applied to the deal.', 'wcd'); ?></p>
                        </div>

                        <div class="input-group">
                            <label for="deal_expire"><?php _e('Deal expire date', 'wcd'); ?> <span class="required">*</span></label>
                            <input type="text" name="deal_expire" id="deal_expire" value="<?php echo!empty($deal_expire) && $deal_expire !== '-1' ? date('Y-m-d', $deal_expire) : '' ?>" class="form-control" readonly="readonly" data-range="<?php echo esc_attr($date_range); ?>" data-validation="required" data-error="<?php esc_attr_e('Please input deal expiration date', 'wcd') ?>" >
                            <p class="description"><?php _e('Set expire date for the deal.', 'wcd'); ?></p>
                        </div>
                        <div class="input-group">
                            <label for="deal_price"><?php _e('Deal price', 'wcd'); ?> <span class="required">*</span></label>
                            <input type="text" name="deal_price" id="deal_price" value="<?php echo esc_attr($deal_price) ?>" class="form-control" data-validation="required" data-error="<?php esc_attr_e('Please input real deal price', 'wcd') ?>">
                            <p class="description"><?php _e('Input real price of the deal without currency simbol. If this value is decimal than use . as decimal separator and max two decimal places.', 'wcd'); ?></p>
                        </div>
                        <div class="input-group">
                            <label for="deal_sale_price"><?php _e('Deal sale price', 'wcd'); ?> <span class="required">*</span></label>
                            <input type="text" name="deal_sale_price" id="deal_sale_price" value="<?php echo esc_attr($deal_sale_price) ?>" class="form-control" data-validation="required" data-error="<?php esc_attr_e('Please input deal sale price', 'wcd') ?>">
                            <p class="description"><?php _e('Input sale price of the deal without currency simbol ( auto updated by the percentage change in the next field ). If this value is decimal than use . as decimal separator and max two decimal places.', 'wcd'); ?></p>
                        </div>
                        <div class="input-group">
                            <label for="deal_voucher_expire"><?php _e('Deal voucher expire number of days', 'wcd'); ?> </label>
                            <input type="text" name="deal_voucher_expire" id="deal_voucher_expire" value="<?php echo esc_attr($deal_voucher_expire) ?>" class="form-control"  >
                            <p class="description"><?php _e('Set number of days after which voucher is expire after purchase or leave empty for unlimited last.', 'wcd'); ?></p>
                        </div>
                        <div class="input-group">
                            <label for="deal_items"><?php _e('Deal items', 'wcd'); ?> <span class="required">*</span></label>
                            <input type="text" name="deal_items" id="deal_items" value="<?php echo esc_attr($deal_items) ?>" class="form-control" data-validation="required" data-error="<?php esc_attr_e('Please input number of deal items you wish to sell', 'wcd') ?>">
                            <p class="description"><?php _e('Input number of deal items or services which will be available for purchase.', 'wcd'); ?></p> 
                        </div>
                        <div class="input-group">
                            <label for="deal_item_vouchers"><?php _e('Deal item vouchers', 'wcd'); ?></label>
                            <textarea name="deal_item_vouchers" id="deal_item_vouchers" class="form-control" data-validation="length_conditional" data-field_number_val="#deal_items" data-error="<?php esc_attr_e('You need to input number of vouchers same as inputed deal item.', 'wcd') ?>"><?php echo esc_attr($deal_item_vouchers) ?></textarea>
                            <p class="description"><?php _e('If you want to serve predefined vouchers instead of random generated ones, input them here one in a row and make sure that you have same amount of these vouchers as the number of items.', 'wcd'); ?></p>
                        </div>
                    </div>
                    <div id="deal-variations">
                        <div class="input-group">
                            <label for="deal_variations"><?php _e('Deal Variations', 'wcd'); ?> </label>
                            <a href="javascript:;" class="btn new-variation" title="<?php esc_attr_e('Add new variation', 'wcd'); ?>"><i class="fa fa-plus"></i> <?php _e('Add new variation', 'wcd'); ?></a>
                            <div class="variation-wrap">
                                <div class="title">
                                    <div class="input-group">
                                        <label for="deal_variation_title"><?php _e('Variation Title', 'wcd'); ?></label>            
                                        <input type="text" name="deal_variations[title][]" id="deal_variation_title" class="form-control">
                                    </div>        
                                </div>
                                <div class="description">
                                    <div class="input-group">
                                        <label for="deal_variation_description"><?php _e('Variation Description', 'wcd'); ?> </label>            
                                        <input type="text" name="deal_variations[description][]" id="deal_variation_description" class="form-control">
                                    </div>
                                </div>
                                <div class="price">
                                    <div class="input-group">
                                        <label for="deal_variation_price"><?php _e('Variation price', 'wcd'); ?></label>
                                        <input type="text" name="deal_variations[price][]" id="deal_variation_price" class="form-control">
                                        <p class="description"><?php _e('Input real price of the variation without currency simbol. If this value is decimal than use . as decimal separator and max two decimal places.', 'wcd'); ?></p>
                                    </div>
                                </div>
                                <div class="sale-price">
                                    <div class="input-group">
                                        <label for="deal_variation_sale_price"><?php _e('Variation sale price', 'wcd'); ?></label>
                                        <input type="text" name="deal_variations[sale_price][]" id="deal_variation_sale_price" class="form-control">
                                        <p class="description"><?php _e('Input sale price of the variation without currency simbol ( auto updated by the percentage change in the next field ). If this value is decimal than use . as decimal separator and max two decimal places.', 'wcd'); ?></p>
                                    </div>
                                </div>
                                <div class="remove">
                                    <a href="javascript:;" class="btn remove-variation" title="<?php esc_attr_e('Remove variation', 'wcd'); ?>"><i class="fa fa-minus"></i> <?php _e('Remove variation', 'wcd'); ?></a>
                                </div>        
                            </div>
                            <p class="description"><?php _e('Input deal variations. It will appear in add to cart form on single product page. Leave empty if you not need variations.', 'wcd'); ?></p>
                        </div>                        
                    </div>
                    <div id="deal-appearance">
                        <div class="input-group">
                            <label for="deal_cat"><?php _e('Deal category', 'wcd'); ?> <span class="required">*</span></label>
                            <select name="deal_cat" id="deal_cat" class="form-control" data-validation="conditional" data-conditional-field="deal_new_category" data-error="<?php esc_attr_e('Please select deal category', 'wcd'); ?>">
                                <option value=""><?php _e('- Select -', 'wcd') ?></option>
                                <?php
                                $categories = get_terms('product_cat', array('hide_empty' => false));
                                if (!empty($categories)) {
                                    foreach ($categories as $key => $category) {
                                        wcd_display_select_tree($category, $deal_cat);
                                    }
                                }
                                ?>
                            </select>
                            <p class="description"><?php _e('Select category for the deal or populate field bellow to request new one leaving this field not selected.', 'wcd'); ?></p>
                        </div>
                        <div class="input-group">
                            <label for="deal_new_category"><?php _e('Category not listed?', 'wcd'); ?></label>
                            <input type="text" name="deal_new_category" id="deal_new_category" class="form-control" value="<?php echo $deal_new_category; ?>">
                            <p class="description"><?php _e('Populate this field if desired category is not listed in the previous field.', 'wcd'); ?></p>
                        </div>

                        <div class="input-group">
                            <label for="deal_location"><?php _e('Deal location', 'wcd'); ?> <span class="required">*</span></label>
                            <select name="deal_location" id="deal_location" class="form-control" data-validation="conditional" data-conditional-field="deal_new_location" data-error="<?php esc_attr_e('Please select deal location', 'wcd'); ?>">
                                <option value=""><?php _e('- Select -', 'wcd') ?></option>
                                <?php
                                $locations = get_terms('location', array('hide_empty' => false));

                                if (!empty($locations)) {
                                    foreach ($locations as $key => $location) {
                                        wcd_display_select_tree($location, $deal_location);
                                    }
                                }
                                ?>
                            </select>
                            <p class="description"><?php _e('Select location for the deal or populate field bellow to request new one leaving this field not selected.', 'wcd'); ?></p>
                        </div>
                        <div class="input-group">
                            <label for="deal_new_location"><?php _e('Location not listed?', 'wcd'); ?></label>
                            <input type="text" name="deal_new_location" id="deal_new_location" class="form-control" value="<?php echo $deal_new_location; ?>" >
                            <p class="description"><?php _e('Populate this field if desired location is not listed in the previous field.', 'wcd'); ?></p>
                        </div>
                    </div>

                    <div id="deal-misc">
                        <div class="input-group">
                            <label for="deal_markers"><?php _e('Deal Marker Locations', 'wcd'); ?> </label>
                            <a href="javascript:;" class="btn new-marker" title="<?php esc_attr_e('Add new marker', 'wcd'); ?>"><i class="fa fa-plus"></i> <?php _e('Add new marker', 'wcd'); ?></a>
                            <div class="marker-wrap">
                                <input type="text" name="deal_markers[deal_marker_latitude][]" id="deal_marker_latitude" class="form-control">
                                <input type="text" name="deal_markers[deal_marker_longitude][]" id="deal_marker_longitude" class="form-control">
                                <div class="map"></div>
                                <div class="remove">
                                    <a href="javascript:;" class="btn remove-marker" title="<?php esc_attr_e('Remove marker', 'wcd'); ?>"><i class="fa fa-minus"></i> <?php _e('Remove marker', 'wcd'); ?></a>
                                </div>        
                            </div>
                            <p class="description"><?php _e('Set places where customers can use their vauchers.', 'wcd'); ?></p>
                        </div>
                        <div class="input-group">
                            <label for="deal_link"><?php _e('Deal affiliate link', 'wcd'); ?> </label>
                            <input type="text" name="deal_link" id="deal_link" value="<?php echo esc_attr($deal_link) ?>" class="form-control">
                            <p class="description"><?php _e('Input affiliate link for the deal in order to avoid payment over this website.', 'wcd'); ?></p>
                        </div>
                    </div>

                    <?php wp_nonce_field('submit-deal', 'wcd-deal-nonce'); ?>
                    <div id="deal-wizard-navigation">
                        <a href="#" class="deal-prev"><?php _e('Previous', 'wcd'); ?></a>
                        <a href="#" class="deal-next"><?php _e('Next', 'wcd'); ?></a>
                        <input type="submit" class="deal-submit" name="deal_submit" value="<?php _e('Submit', 'wcd'); ?>"/>                        
                    </div>
                </div>
            </form>
            <?php
        } else {
            show_deals();
        }
        break;
    case 'remove':
        $deal = get_post($deal_id);
        if (get_current_user_id() == $deal->post_author) {
            //wp_delete_post($deal_id, true);
            //wc_delete_product_transients($deal_id);
            $deal->post_status = 'private';
            wp_update_post($deal);
            wc_add_notice(__('Deal is deleted', 'wcd'), 'success');
            show_deals();
        }
        break;
    case 'vouchers':
        switch ($subaction) {
            case 'used':
            case 'not_used':
                wcd_set_voucher_status($item_id, $subaction);
                wc_add_notice(__('Voucher status updated', 'wcd'), 'success');
            default:
                if (is_numeric($deal_id)) {
                    $vouchers = wcd_get_vouchers($deal_id);
                    $deal = get_post($deal_id);
                    wc_print_notices();
                    ?>
                    <div id="deal-toolbar" class="btn-group">
                        <a href="<?php echo esc_url(remove_query_arg(array('action', 'subaction', 'deal_id', 'item_id'))); ?>" class="btn btn-default">
                            <?php _e('Back to deals', 'wcd') ?>
                        </a>
                    </div>
                    <h3><?php _e('Voucher list for deal', 'wcd') ?>: <a href="<?php echo get_permalink($deal_id) ?>" target="_blank"><?php echo $deal->post_title; ?></a></h3>
                    <table data-toggle="table" data-search="true">
                        <thead>
                            <tr>
                                <th data-field="voucher_code" data-sortable="true" data-classes="table table-striped">
                                    <?php _e('Voucher Code', 'wcd'); ?>
                                </th>
                                <th data-field="status" data-sortable="true">
                                    <?php _e('Status', 'wcd'); ?>
                                </th>      
                                <th data-field="action" data-sortable="true">
                                    <?php _e('Action', 'wcd'); ?>
                                </th>	        
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (is_array($vouchers)) {
                                foreach ($vouchers as $voucher) {
                                    if (empty($voucher->item_id))
                                        continue;

                                    $expired = false;
                                    if (is_numeric($voucher->deal_voucher_expire)) {
                                        if (strtotime($voucher->post_date) + $voucher->deal_voucher_expire * 24 * 60 * 60 <= current_time('timestamp')) {
                                            $expired = true;
                                        }
                                    }
                                    ?>
                                    <tr class="<?php echo $expired ? 'disabled' : '' ?>">
                                        <td>
                                            <?php echo str_replace('/', '', $voucher->voucher_code); ?>
                                        </td>
                                        <td>
                                            <?php
                                            if ($expired) {
                                                _e('Expired', 'wcd');
                                            } else {
                                                if (strpos($voucher->voucher_code, '/') === false) {
                                                    _e('Not Used', 'wcd');
                                                } else {
                                                    _e('Used', 'wcd');
                                                }
                                            }
                                            ?>
                                        </td>
                                        <td>
                                            <a title="<?php esc_attr_e('Mark as used', 'wcd') ?>" href="<?php echo esc_url(add_query_arg(array('deal_id' => $deal_id, 'item_id' => $voucher->item_id, 'action' => 'vouchers', 'subaction' => 'used'))); ?>" class="voucher-mark">
                                                <i class="fa fa-check-circle"></i>
                                            </a>
                                            <a title="<?php esc_attr_e('Mark as not used', 'wcd') ?>" href="<?php echo esc_url(add_query_arg(array('deal_id' => $deal_id, 'item_id' => $voucher->item_id, 'action' => 'vouchers', 'subaction' => 'not_used'))); ?>" class="voucher-mark">
                                                <i class="fa fa-times-circle"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                        </tbody>
                    </table>


                    <?php
                }
        }
        break;
    default:
        show_deals();
}



