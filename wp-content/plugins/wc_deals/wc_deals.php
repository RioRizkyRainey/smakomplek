<?php
/*
  Plugin Name: WooCommerce Deals
  Plugin URI: http://azexo.com
  Description: WooCommerce Deals
  Text Domain: wcd
  Domain Path: /lang
  Version: 1.0
  Author: azexo
  Author URI: http://azexo.com
  License: GNU General Public License version 3.0
 */

add_action('plugins_loaded', 'wcd_plugins_loaded');

function wcd_plugins_loaded() {
    load_plugin_textdomain('wcd', FALSE, basename(dirname(__FILE__)) . '/lang/');
}

if (function_exists('is_woocommerce_activated') && is_woocommerce_activated()) {


    define('WCD_URL', plugins_url('', __FILE__));
    define('WCD_DIR', trailingslashit(dirname(__FILE__)) . '/');

    add_action('init', 'wcd_init');

    function wcd_init() {

        add_shortcode('wcd_vendor_deal_edit', 'wcd_vendor_deal_edit');

        register_taxonomy('location', array('product'), array(
            'label' => __('Location', 'wcd'),
            'hierarchical' => true,
            'labels' => array(
                'name' => __('Location', 'wcd'),
                'singular_name' => __('Location', 'wcd'),
                'menu_name' => __('Location', 'wcd'),
                'all_items' => __('All Locations', 'wcd'),
                'edit_item' => __('Edit Location', 'wcd'),
                'view_item' => __('View Location', 'wcd'),
                'update_item' => __('Update Location', 'wcd'),
                'add_new_item' => __('Add New Location', 'wcd'),
                'new_item_name' => __('New Location Name', 'wcd'),
                'parent_item' => __('Parent Location', 'wcd'),
                'parent_item_colon' => __('Parent Location:', 'wcd'),
                'search_items' => __('Search Locations', 'wcd'),
                'popular_items' => __('Popular Locations', 'wcd'),
                'separate_items_with_commas' => __('Separate locations with commas', 'wcd'),
                'add_or_remove_items' => __('Add or remove locations', 'wcd'),
                'choose_from_most_used' => __('Choose from the most used locations', 'wcd'),
                'not_found' => __('No locations found', 'wcd'),
            )
        ));

        $role = get_role('vendor');
        if ($role) {
            $role->add_cap('upload_files');
        }
        if (!current_user_can('administrator') && !is_admin()) {
            show_admin_bar(false);
        }
    }

    add_filter('user_has_cap', 'wcd_user_has_cap', 10, 4);

    function wcd_user_has_cap($allcaps, $caps, $args, $user) {
        if (count($args) == 3) {
            if ($args[0] == 'edit_post') {
                if ($args[1] == get_current_user_id()) {
                    if (in_array('vendor', $user->roles)) {
                        $submit_post = get_post($args[2]);
                        if (strpos($submit_post->post_content, '[wcd_vendor_deal_edit]') !== false) {
                            $allcaps['edit_others_pages'] = 1;
                            $allcaps['edit_published_pages'] = 1;
                        }
                    }
                }
            }
        }
        return $allcaps;
    }

    add_action('admin_enqueue_scripts', 'wcd_admin_enqueue_scripts');

    function wcd_admin_enqueue_scripts() {
        wp_enqueue_style('wcd-backend', WCD_URL . '/css/backend.css');
    }

    add_action('wp_enqueue_scripts', 'wcd_enqueue_scripts', 11);

    function wcd_enqueue_scripts() {
        if (function_exists('woo_vou_plugin_loaded')) {
            wp_dequeue_style('woo-vou-public-style');
        }
    }

    add_action('woocommerce_product_write_panel_tabs', 'wcd_write_panel_tabs');

    function wcd_write_panel_tabs() {
        ?>
        <li class="deal"><a href="#deal"><?php _e('Deal', 'wcd'); ?></a></li>
        <li class="vendor-requests"><a href="#vendor-requests"><?php _e('Vendor requests', 'wcd'); ?></a></li>
        <?php
    }

    add_action('woocommerce_product_data_panels', 'wcd_data_panels');

    function wcd_data_panels() {
        ?>
        <div id="deal" class="panel woocommerce_options_panel"><div class="options_group">
                <?php
                if (!function_exists('woo_vou_plugin_loaded')) {
                    woocommerce_wp_text_input(
                            array(
                                'id' => 'deal_voucher_expire',
                                'label' => __('Vouchers Expire (days)', 'wcd'),
                                'placeholder' => '',
                                'desc_tip' => 'true',
                                'description' => __('Set number of days after which voucher is expire after purchase or leave empty for unlimited last.', 'wcd'),
                                'type' => 'decimal'
                    ));
                }

                woocommerce_wp_textarea_input(
                        array(
                            'id' => 'deal_markers',
                            'label' => __('Markers', 'wcd'),
                            'desc_tip' => 'true',
                            'description' => __('Set places where customers can use their vauchers. Use comma separated Latitude & Longitude for google map markers. One line - one marker.', 'wcd'),
                ));

                if (!function_exists('woo_vou_plugin_loaded')) {
                    woocommerce_wp_textarea_input(
                            array(
                                'id' => 'deal_item_vouchers',
                                'label' => __('Items Vouchers', 'wcd'),
                                'desc_tip' => 'true',
                                'description' => __('If you want to serve predefined vouchers instead of random generated ones, input them here one in a row and make sure that you have same amount of these vouchers as the number of items.', 'wcd'),
                    ));
                }
                ?>
            </div></div>
        <div id="vendor-requests" class="panel woocommerce_options_panel"><div class="options_group">
                <?php
                woocommerce_wp_text_input(
                        array(
                            'id' => 'deal_new_category',
                            'label' => __('New category request', 'wcd'),
                            'placeholder' => '',
                            'desc_tip' => 'true',
                            'description' => __('Request for new category is listed here.', 'wcd'),
                ));

                woocommerce_wp_text_input(
                        array(
                            'id' => 'deal_new_location',
                            'label' => __('New location request', 'wcd'),
                            'placeholder' => '',
                            'desc_tip' => 'true',
                            'description' => __('Request for new location is listed here.', 'wcd'),
                ));
                ?>
            </div></div>
        <?php
    }

    add_action('woocommerce_process_product_meta', 'wcd_save_custom_settings');

    function wcd_save_custom_settings($post_id) {

        if (!function_exists('woo_vou_plugin_loaded')) {
            $deal_voucher_expire = $_POST['deal_voucher_expire'];
            if (!empty($deal_voucher_expire)) {
                update_post_meta($post_id, 'deal_voucher_expire', esc_attr($deal_voucher_expire));
            }
        }

        $deal_markers = $_POST['deal_markers'];
        if (!empty($deal_markers)) {
            update_post_meta($post_id, 'deal_markers', esc_attr($deal_markers));
        }

        if (!function_exists('woo_vou_plugin_loaded')) {
            $deal_item_vouchers = $_POST['deal_item_vouchers'];
            if (!empty($deal_item_vouchers)) {
                update_post_meta($post_id, 'deal_item_vouchers', esc_attr($deal_item_vouchers));
            }
        }

        $deal_new_category = $_POST['deal_new_category'];
        if (!empty($deal_new_category)) {
            update_post_meta($post_id, 'deal_new_category', esc_attr($deal_new_category));
        }

        $deal_new_location = $_POST['deal_new_location'];
        if (!empty($deal_new_location)) {
            update_post_meta($post_id, 'deal_new_location', esc_attr($deal_new_location));
        }
    }

    function wcd_generate_voucher($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        global $wpdb;
        $random_string = '';
        for ($i = 0; $i < $length; $i++) {
            $random_string .= $characters[rand(0, strlen($characters) - 1)];
        }

        $exists = $wpdb->get_results($wpdb->prepare("SELECT * FROM {$wpdb->order_itemmeta} WHERE meta_key = 'Voucher code' AND meta_value = %s", $random_string));
        $exists = array_shift($exists);
        if (!empty($exists)) {
            $random_string = wcd_generate_voucher();
        } else {
            return $random_string;
        }
    }

    add_action('woocommerce_payment_complete', 'wcd_payment_complete');
    add_action('woocommerce_order_status_completed', 'wcd_payment_complete');

    function wcd_payment_complete($id) {
        if (!function_exists('woo_vou_plugin_loaded')) {
            $order = new WC_Order($id);
            $items = $order->get_items();
            foreach ($items as $item_id => $item) {
                if ($item['type'] == 'line_item') {
                    $codes = wc_get_order_item_meta($item_id, 'Voucher code', false);
                    if (!$codes || empty($codes)) {
                        for ($i = 0; $i < $item['qty']; $i++) {
                            wc_add_order_item_meta($item_id, 'Voucher code', wcd_generate_voucher());
                        }
                    }
                }
            }
        }
    }

    if (!class_exists('WP_List_Table')) {
        require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
    }

    class Vouchers_Table extends WP_List_Table {

        function __construct() {

            $this->index = 0;
            parent::__construct(array(
                'singular' => 'voucher',
                'plural' => 'vouchers',
                'ajax' => false
            ));
        }

        function get_columns() {
            $columns = array(
                'cb' => '<input type="checkbox" />',
                'order' => __('Order', 'wcd'),
                'deal' => __('Deal', 'wcd'),
                'voucher_code' => __('Voucher Code', 'wcd'),
                'vendor' => __('Vendor', 'wcd'),
                'buyer' => __('Buyer', 'wcd'),
                'date' => __('Date', 'wcd'),
            );
            return $columns;
        }

        function column_cb($item) {
            return sprintf(
                    '<input type="checkbox" name="%1$s[]" value="%2$s" />',
                    /* $1%s */ 'id',
                    /* $2%s */ $item->id
            );
        }

        function prepare_items() {
            global $wpdb;

            $_SERVER['REQUEST_URI'] = remove_query_arg('_wp_http_referer', $_SERVER['REQUEST_URI']);

            $per_page = $this->get_items_per_page('voucher_per_page', 10);
            $current_page = $this->get_pagenum();

            $orderby = !empty($_REQUEST['orderby']) ? esc_attr($_REQUEST['orderby']) : 'date';
            $order = (!empty($_REQUEST['order']) && $_REQUEST['order'] == 'asc' ) ? 'ASC' : 'DESC';

            $this->_column_headers = $this->get_column_info();


            $sql = "FROM {$wpdb->order_itemmeta} as v "
                    . "LEFT JOIN {$wpdb->prefix}woocommerce_order_items as i ON i.order_item_id = v.order_item_id "
                    . "LEFT JOIN {$wpdb->posts} as o ON i.order_id = o.id "
                    . "LEFT JOIN {$wpdb->postmeta} as m ON m.post_id = o.id "
                    . "LEFT JOIN {$wpdb->users} as u ON m.meta_value = u.id "
                    . "LEFT JOIN {$wpdb->order_itemmeta} as p ON p.order_item_id = i.order_item_id "
                    . "LEFT JOIN {$wpdb->posts} as pp ON pp.id = p.meta_value "
                    . "LEFT JOIN {$wpdb->users} as pv ON pv.id = pp.post_author "
                    . "WHERE v.meta_key = 'Voucher code' "
                    . "AND p.meta_key = '_product_id' "
                    . "AND m.meta_key = '_customer_user'";

            $max = $wpdb->get_var("SELECT COUNT(*) " . $sql);


            $sql = "SELECT v.meta_value as id, i.order_id as order_id, p.meta_value as deal_id, pp.post_title as deal_title, REPLACE(v.meta_value, '/', '') as voucher_code, o.post_date as date, u.user_login as buyer_name, u.id as buyer_id, pv.user_login as vendor_name, pv.id as vendor_id " . $sql;


            $offset = ( $current_page - 1 ) * $per_page;

            $sql .= " ORDER BY `{$orderby}` {$order} LIMIT {$offset}, {$per_page}";

            $this->items = $wpdb->get_results($sql);

            $this->set_pagination_args(array(
                'total_items' => $max,
                'per_page' => $per_page,
                'total_pages' => ceil($max / $per_page)
            ));
        }

        function get_sortable_columns() {
            $sortable_columns = array(
                'order' => array('order', false),
                'deal' => array('deal', false),
                'vendor' => array('vendor', false),
                'buyer' => array('buyer', false),
                'date' => array('date', true),
            );
            return $sortable_columns;
        }

        function column_default($item, $column_name) {
            switch ($column_name) {
                case 'order':
                    return '<a href="' . admin_url('post.php?post=' . $item->order_id . '&action=edit') . '">' . $item->order_id . '</a>';
                case 'deal':
                    $parent = get_post_ancestors($item->deal_id);
                    $product_id = $parent ? $parent[0] : $item->deal_id;
                    return '<a href="' . admin_url('post.php?post=' . $product_id . '&action=edit') . '">' . $item->deal_title . '</a>';
                case 'voucher_code':
                    return $item->voucher_code;
                case 'vendor':
                    return '<a href="' . admin_url('user-edit.php?user_id=' . $item->vendor_id) . '">' . $item->vendor_name . '</a>';
                case 'buyer':
                    return '<a href="' . admin_url('user-edit.php?user_id=' . $item->buyer_id) . '">' . $item->buyer_name . '</a>';
                case 'date':
                    return date_i18n(get_option('date_format'), strtotime($item->date));
            }
        }

    }

    add_action('admin_menu', 'wcd_add_menu_items');

    function wcd_add_menu_items() {
        if (!function_exists('woo_vou_plugin_loaded')) {
            $hook = add_menu_page(__('Deals vouchers', 'wcd'), __('Deals vouchers', 'wcd'), 'view_woocommerce_reports', 'vouchers', 'wcd_vouchers', 'dashicons-tickets-alt', 30);
            add_action("load-$hook", 'wcd_vouchers_add_options');
        }
    }

    function wcd_vouchers_add_options() {
        global $VouchersTable;

        $args = array(
            'label' => 'Rows',
            'default' => 10,
            'option' => 'vouchers_per_page'
        );
        add_screen_option('per_page', $args);

        $VouchersTable = new Vouchers_Table();
    }

    function wcd_vouchers() {
        global $VouchersTable;

        echo '<div class="wrap"><h2>' . __('Deals vouchers', 'wcd') . '</h2>';

        $VouchersTable->prepare_items();
        $VouchersTable->display();

        echo '</div>';
    }

    function wcd_display_select_tree($term, $selected = '', $level = 0) {
        if (!empty($term->children)) {
            echo '<option value="" disabled>' . str_repeat('&nbsp;&nbsp;', $level) . '' . $term->name . '</option>';
            $level++;
            foreach ($term->children as $key => $child) {
                wcd_display_select_tree($child, $selected, $level);
            }
        } else {
            echo '<option value="' . $term->term_id . '" ' . ( $term->term_id == $selected ? 'selected="selected"' : '' ) . '>' . str_repeat('&nbsp;&nbsp;', $level) . '' . $term->name . '</option>';
        }
    }

    function wcd_set_voucher_status($item_id, $status) {
        $voucher_code = wc_get_order_item_meta($item_id, 'Voucher code');
        $voucher_code = str_replace('/', '', $voucher_code);
        if ($status == 'used') {
            wc_update_order_item_meta($item_id, 'Voucher code', '/' . $voucher_code);
        }
        if ($status == 'not_used') {
            wc_update_order_item_meta($item_id, 'Voucher code', $voucher_code);
        }
    }

    function wcd_get_vouchers($deal_id) {
        global $wpdb;
        $sql = "SELECT i.order_id as order_id, i.order_item_id as item_id, m.meta_value as deal_voucher_expire, o.post_date, v.meta_value as voucher_code "
                . "FROM {$wpdb->order_itemmeta} as v "
                . "LEFT JOIN {$wpdb->prefix}woocommerce_order_items as i ON i.order_item_id = v.order_item_id "
                . "LEFT JOIN {$wpdb->posts} as o ON i.order_id = o.id "
                . "LEFT JOIN {$wpdb->order_itemmeta} as p ON p.order_item_id = i.order_item_id "
                . "LEFT JOIN {$wpdb->posts} as pp ON pp.id = p.meta_value "
                . "LEFT JOIN {$wpdb->postmeta} as m ON m.post_id = pp.id "
                . "WHERE v.meta_key = 'Voucher code' "
                . "AND p.meta_key = '_product_id' "
                . "AND m.meta_key = 'deal_voucher_expire' "
                . "AND pp.id = %d "
                . "AND pp.post_author = %d";
        return $wpdb->get_results($wpdb->prepare($sql, $deal_id, get_current_user_id()));
    }

    function wcd_get_deals() {
        global $wpdb;
        $sql = "SELECT p.id as deal_id, p.post_title as deal_title, p.post_status as status, e.meta_value as deal_expire, m.meta_value as stock, GROUP_CONCAT(DISTINCT t.name) as category, SUM(q.meta_value) as purchases "
                . "FROM {$wpdb->posts} as p "
                . "LEFT JOIN {$wpdb->postmeta} as m ON m.post_id = p.id "
                . "LEFT JOIN {$wpdb->postmeta} as e ON e.post_id = p.id "
                . "LEFT JOIN {$wpdb->prefix}term_relationships tr ON p.ID = tr.object_id "
                . "LEFT JOIN {$wpdb->prefix}term_taxonomy as tt ON tt.term_taxonomy_id = tr.term_taxonomy_id "
                . "LEFT JOIN {$wpdb->prefix}terms as t ON t.term_id = tt.term_id "
                . "LEFT JOIN {$wpdb->order_itemmeta} as i ON p.id = i.meta_value "
                . "LEFT JOIN {$wpdb->order_itemmeta} as q ON q.order_item_id = i.order_item_id "
                . "LEFT JOIN {$wpdb->prefix}woocommerce_order_items as oi ON oi.order_item_id = i.order_item_id "
                . "LEFT JOIN {$wpdb->posts} as o ON o.id = oi.order_id "
                . "WHERE (p.post_status = 'publish' || p.post_status = 'pending') "
                . "AND (tt.taxonomy = 'product_cat' OR tt.taxonomy IS NULL) "
                . "AND m.meta_key = '_stock' "
                . "AND e.meta_key = '_sale_price_dates_to' "
                . "AND (i.meta_key = '_product_id' OR i.meta_key IS NULL) "
                . "AND (q.meta_key = '_qty' OR q.meta_key IS NULL) "
                . "AND (o.post_status = 'wc-completed' OR o.post_status IS NULL) "
                . "AND p.post_author = %d "
                . "GROUP BY p.id";
        return $wpdb->get_results($wpdb->prepare($sql, get_current_user_id()));
    }

    function wcd_get_deal_purchases($id) {
        global $wpdb;
        $sql = "SELECT SUM(q.meta_value) as purchases "
                . "FROM {$wpdb->posts} as p "
                . "LEFT JOIN {$wpdb->order_itemmeta} as i ON p.id = i.meta_value "
                . "LEFT JOIN {$wpdb->order_itemmeta} as q ON q.order_item_id = i.order_item_id "
                . "LEFT JOIN {$wpdb->prefix}woocommerce_order_items as oi ON oi.order_item_id = i.order_item_id "
                . "LEFT JOIN {$wpdb->posts} as o ON o.id = oi.order_id "
                . "WHERE p.post_status = 'publish' "
                . "AND p.id = %d "
                . "AND (i.meta_key = '_product_id' OR i.meta_key IS NULL) "
                . "AND (q.meta_key = '_qty' OR q.meta_key IS NULL) "
                . "AND (o.post_status = 'wc-completed' OR o.post_status IS NULL)";
        $purchased = $wpdb->get_var($wpdb->prepare($sql, $id));
        if (empty($purchased))
            $purchased = 0;
        return $purchased;
    }

    add_action('woocommerce_scheduled_sales', 'wcd_scheduled_sales', 11);

    function wcd_scheduled_sales() {
        global $wpdb;
        $product_ids = $wpdb->get_col("
		SELECT postmeta.post_id FROM {$wpdb->postmeta} as postmeta
		WHERE (postmeta.meta_key = '_sale_price_dates_to' AND postmeta.meta_value = '')
                OR (postmeta.meta_key = '_sale_price_dates_from' AND postmeta.meta_value = '')
	");
        if ($product_ids) {
            foreach ($product_ids as $product_id) {
                $product = wc_get_product($product_id);
                if ($product->is_type('variable')) {
                    $children = $product->get_children();
                    wcd_variable_product_sync($product_id, $children);
                }
            }
        }
    }

    add_action('woocommerce_api_process_product_meta_variable', 'wcd_api_process_product_meta_variable', 10, 2);

    function wcd_api_process_product_meta_variable($product_id, $data) {
        $product = wc_get_product($product_id);
        $children = $product->get_children();
        wcd_variable_product_sync($product_id, $children);
    }

    add_action('woocommerce_variable_product_sync', 'wcd_variable_product_sync', 10, 2);

    function wcd_variable_product_sync($product_id, $children) {
        $min_from = null;
        $max_to = null;
        foreach ($children as $child_id) {
            $from = get_post_meta($child_id, '_sale_price_dates_from', true);
            if (is_numeric($from)) {
                if (is_null($min_from)) {
                    $min_from = $from;
                } else {
                    if ($min_from > $from) {
                        $min_from = $from;
                    }
                }
            }
            $to = get_post_meta($child_id, '_sale_price_dates_to', true);
            if (is_numeric($to)) {
                if (is_null($max_to)) {
                    $max_to = $to;
                } else {
                    if ($max_to < $to) {
                        $max_to = $to;
                    }
                }
            }
        }
        if (!is_null($min_from)) {
            update_post_meta($product_id, '_sale_price_dates_from', $min_from);
        }
        if (!is_null($max_to)) {
            update_post_meta($product_id, '_sale_price_dates_to', $max_to);
        }
    }

    function wcd_add_update_attributes($post_id, $attribute_names, $attribute_values) {
        $attribute_names_max_key = max(array_keys($attribute_names));
        $is_visible = 1;
        $is_variation = 1;
        $is_taxonomy = 0;
        $attributes = array();
        for ($i = 0; $i <= $attribute_names_max_key; $i++) {
            if ($is_taxonomy) {

                $values_are_slugs = false;

                if (isset($attribute_values[$i])) {

                    // Select based attributes - Format values (posted values are slugs)
                    if (is_array($attribute_values[$i])) {
                        $values = array_map('sanitize_title', $attribute_values[$i]);
                        $values_are_slugs = true;

                        // Text based attributes - Posted values are term names - don't change to slugs
                    } else {
                        $values = array_map('stripslashes', array_map('strip_tags', explode(WC_DELIMITER, $attribute_values[$i])));
                    }

                    // Remove empty items in the array
                    $values = array_filter($values, 'strlen');
                } else {
                    $values = array();
                }

                // Update post terms
                if (taxonomy_exists($attribute_names[$i])) {

                    foreach ($values as $key => $value) {
                        $term = get_term_by($values_are_slugs ? 'slug' : 'name', trim($value), $attribute_names[$i]);

                        if ($term) {
                            $values[$key] = intval($term->term_id);
                        } else {
                            $term = wp_insert_term(trim($value), $attribute_names[$i]);
                            if (isset($term->term_id)) {
                                $values[$key] = intval($term->term_id);
                            }
                        }
                    }

                    wp_set_object_terms($post_id, $values, $attribute_names[$i]);
                }

                if (!empty($values)) {
                    // Add attribute to array, but don't set values
                    $attributes[sanitize_title($attribute_names[$i])] = array(
                        'name' => wc_clean($attribute_names[$i]),
                        'value' => '',
                        'position' => 0,
                        'is_visible' => $is_visible,
                        'is_variation' => $is_variation,
                        'is_taxonomy' => $is_taxonomy
                    );
                }
            } elseif (isset($attribute_values[$i])) {

                // Text based, separate by pipe
                $values = implode(' ' . WC_DELIMITER . ' ', array_map('wc_clean', wc_get_text_attributes($attribute_values[$i])));

                // Custom attribute - Add attribute to array and set the values
                $attributes[sanitize_title($attribute_names[$i])] = array(
                    'name' => wc_clean($attribute_names[$i]),
                    'value' => $values,
                    'position' => 0,
                    'is_visible' => $is_visible,
                    'is_variation' => $is_variation,
                    'is_taxonomy' => $is_taxonomy
                );
            }
        }
        update_post_meta($post_id, '_product_attributes', $attributes);
    }

    function wcd_add_update_variation($post_id, $deal, $variation) {
        $variation_post = array(
            'post_title' => sprintf(__('Variation #%s of %s', 'wcd'), absint($post_id), esc_html(get_the_title($post_id))),
            'post_content' => '',
            'post_status' => 'publish',
            'post_author' => get_current_user_id(),
            'post_parent' => $post_id,
            'post_type' => 'product_variation',
        );
        $variation_id = wp_insert_post($variation_post);
        update_post_meta($variation_id, '_virtual', 'yes');
        if (function_exists('woo_vou_plugin_loaded')) {
            update_post_meta($variation_id, '_downloadable', 'yes');
        }
        update_post_meta($variation_id, '_price', esc_attr($variation['variation_sale_price']));
        update_post_meta($variation_id, '_regular_price', esc_attr($variation['variation_price']));
        update_post_meta($variation_id, '_sale_price', esc_attr($variation['variation_sale_price']));
        update_post_meta($variation_id, '_sale_price_dates_from', strtotime(esc_attr($deal['deal_start'])));
        update_post_meta($variation_id, '_sale_price_dates_to', strtotime(esc_attr($deal['deal_expire'])));
        update_post_meta($variation_id, '_variation_description', wp_kses_post($variation['variation_description']));

        $attributes = maybe_unserialize(get_post_meta($post_id, '_product_attributes', true));
        $updated_attribute_keys = array();
        foreach ($attributes as $attribute) {
            if ($attribute['is_variation']) {
                $attribute_key = 'attribute_' . sanitize_title($attribute['name']);
                $updated_attribute_keys[] = $attribute_key;

                if ($attribute['is_taxonomy']) {
                    // Don't use wc_clean as it destroys sanitized characters
                    $value = isset($variation[$attribute_key]) ? sanitize_title(stripslashes($variation[$attribute_key])) : '';
                } else {
                    $value = isset($variation[$attribute_key]) ? wc_clean(stripslashes($variation[$attribute_key])) : '';
                }

                update_post_meta($variation_id, $attribute_key, $value);
            }
        }

        // Remove old taxonomies attributes so data is kept up to date - first get attribute key names
        global $wpdb;
        $delete_attribute_keys = $wpdb->get_col($wpdb->prepare("SELECT meta_key FROM {$wpdb->postmeta} WHERE meta_key LIKE 'attribute_%%' AND meta_key NOT IN ( '" . implode("','", $updated_attribute_keys) . "' ) AND post_id = %d;", $variation_id));

        foreach ($delete_attribute_keys as $key) {
            delete_post_meta($variation_id, $key);
        }
    }

    function wcd_add_update_deal($deal, $deal_id = false) {
        $post = array(
            'post_title' => esc_attr($deal['deal_title']),
            'post_content' => wp_kses_post($deal['deal_description']),
            'post_excerpt' => wp_kses_post($deal['deal_in_short']),
            'post_status' => (current_user_can('publish_products')) ? 'publish' : 'pending',
            'post_type' => 'product'
        );
        if ($deal_id) {
            $post['ID'] = $deal_id;
            wp_update_post($post);
        } else {
            $deal_id = wp_insert_post($post);
        }

        if (!empty($deal['deal_cat']) && is_numeric($deal['deal_cat'])) {
            wp_set_object_terms($deal_id, intval($deal['deal_cat']), 'product_cat', false);
        }
        if (!empty($deal['deal_location']) && is_numeric($deal['deal_location'])) {
            wp_set_object_terms($deal_id, intval($deal['deal_location']), 'location', false);
        }

        set_post_thumbnail($deal_id, esc_attr($deal['deal_featured_image']));
        if (!empty($deal['deal_images'])) {
            update_post_meta($deal_id, '_product_image_gallery', esc_attr($deal['deal_images']));
        }

        if (empty($deal['deal_link'])) {
            update_post_meta($deal_id, '_virtual', 'yes');
        } else {
            wp_set_object_terms($deal_id, 'external', 'product_type', false);
            update_post_meta($deal_id, '_product_url', esc_attr($deal['deal_link']));
            update_post_meta($deal_id, '_button_text', __('Buy now', 'wcd'));
        }
        update_post_meta($deal_id, '_manage_stock', 'yes');
        update_post_meta($deal_id, '_stock_status', 'instock');
        update_post_meta($deal_id, '_stock', esc_attr($deal['deal_items']));
        update_post_meta($deal_id, '_visibility', 'visible');
        update_post_meta($deal_id, '_price', esc_attr($deal['deal_sale_price']));
        update_post_meta($deal_id, '_regular_price', esc_attr($deal['deal_price']));
        update_post_meta($deal_id, '_sale_price', esc_attr($deal['deal_sale_price']));
        update_post_meta($deal_id, '_sale_price_dates_from', strtotime(esc_attr($deal['deal_start'])));
        update_post_meta($deal_id, '_sale_price_dates_to', strtotime(esc_attr($deal['deal_expire'])));
        if (!empty($deal['deal_markers'])) {
            $deal_markers = array();
            for ($i = 0; $i < sizeof($deal['deal_markers']['deal_marker_longitude']); $i++) {
                if (!empty($deal['deal_markers']['deal_marker_longitude'][$i]) && !empty($deal['deal_markers']['deal_marker_latitude'][$i])) {
                    $deal_markers[] = $deal['deal_markers']['deal_marker_latitude'][$i] . ', ' . $deal['deal_markers']['deal_marker_longitude'][$i];
                }
            }
            update_post_meta($deal_id, 'deal_markers', implode("\n", $deal_markers));
        }
        update_post_meta($deal_id, 'deal_voucher_expire', esc_attr($deal['deal_voucher_expire']));
        update_post_meta($deal_id, 'deal_item_vouchers', esc_attr($deal['deal_item_vouchers']));
        update_post_meta($deal_id, 'deal_new_category', esc_attr($deal['deal_new_category']));
        update_post_meta($deal_id, 'deal_new_location', esc_attr($deal['deal_new_location']));

        if (function_exists('woo_vou_plugin_loaded')) {
            update_post_meta($deal_id, '_downloadable', 'yes');
            update_post_meta($deal_id, '_woo_vou_enable', 'yes');
            update_post_meta($deal_id, '_woo_vou_vendor_user', get_current_user_id());
            if (empty($deal['deal_item_vouchers'])) {
                if (is_numeric($deal['deal_items'])) {
                    $vouchers = array_map(function() {
                        global $woo_vou_model;
                        return $woo_vou_model->woo_vou_get_pattern_string('LLDDD');
                    }, array_fill(0, $deal['deal_items'], ''));
                    update_post_meta($deal_id, '_woo_vou_codes', implode(',', $vouchers));
                    update_post_meta($deal_id, '_woo_vou_using_type', '0');
                } else {
                    update_post_meta($deal_id, '_woo_vou_using_type', '1');
                }
            } else {
                update_post_meta($deal_id, '_woo_vou_codes', implode(',', array_map('trim', explode("\n", $deal['deal_item_vouchers']))));
            }
            if (is_numeric($deal['deal_voucher_expire'])) {
                update_post_meta($deal_id, '_woo_vou_exp_type', 'based_on_purchase');
                update_post_meta($deal_id, '_woo_vou_days_diff', esc_attr($deal['deal_voucher_expire']));
            } else {
                update_post_meta($deal_id, '_woo_vou_exp_type', 'specific_date');
                update_post_meta($deal_id, '_woo_vou_exp_date', esc_attr($deal['deal_expire']));
            }
        }


        $titles = array_filter($deal['deal_variations']['title']);
        $descriptions = array_filter($deal['deal_variations']['description']);
        $sale_prices = array_filter($deal['deal_variations']['sale_price']);
        $prices = array_filter($deal['deal_variations']['price']);
        if (!empty($titles) && !empty($descriptions) && !empty($sale_prices) && !empty($prices)) {
            wp_set_object_terms($deal_id, 'variable', 'product_type', false);
            $attribute_names = array(__('Variation', 'wcd'));
            $attribute_values = array(implode(WC_DELIMITER, $deal['deal_variations']['title']));
            wcd_add_update_attributes($deal_id, $attribute_names, $attribute_values);
            for ($i = 0; $i < count($deal['deal_variations']['title']); $i++) {
                $attribute_key = 'attribute_' . sanitize_title($attribute_names[0]);
                $variation = array(
                    $attribute_key => $deal['deal_variations']['title'][$i],
                    'variation_description' => $deal['deal_variations']['description'][$i],
                    'variation_sale_price' => $deal['deal_variations']['sale_price'][$i],
                    'variation_price' => $deal['deal_variations']['price'][$i],
                );
                wcd_add_update_variation($deal_id, $deal, $variation);
            }
            update_post_meta($deal_id, '_default_attributes', array($attribute_names[0] => $deal['deal_variations']['title'][0]));
        }
        return $deal_id;
    }

    function wcd_vendor_deal_edit($atts) {
        if (!is_user_logged_in() && !WCV_Vendors::is_vendor(get_current_user_id())) {
            $output = '<p>' . __('You need login/register before submit deals. ', 'wcd') . '</p>';
            $output .= '<a href="' . get_permalink(get_option('woocommerce_myaccount_page_id')) . '">' . __('Go to login or register', 'wcd') . '</a>';
            return $output;
        }

        if (WCV_Vendors::is_pending(get_current_user_id())) {
            return '<p>' . __('Your account has not yet been approved to become a vendor.  When it is, you will receive an email telling you that your account is approved!', 'wcd') . '</p>';
        }

        if (!WCV_Vendors::is_vendor(get_current_user_id())) {
            ob_start();
            wc_get_template('denied.php', array(), 'wc-vendors/dashboard/', wcv_plugin_dir . 'templates/dashboard/');

            return ob_get_clean();
        }

        if (isset($_GET['action']) && $_GET['action'] == 'woo_vou_check_code') {
            if (function_exists('woo_vou_plugin_loaded')) {
                $back = '<div id="deal-toolbar" class="btn-group"><a href="' . esc_url(remove_query_arg(array('action'))) . '" class="btn btn-default">' . __('Back to deals', 'wcd') . '</a></div>';
                wp_register_script('woo-vou-check-code-script', WOO_VOU_URL . 'includes/js/woo-vou-check-code.js', array(), WOO_VOU_PLUGIN_VERSION);
                wp_enqueue_script('woo-vou-check-code-script');
                wp_localize_script('woo-vou-check-code-script', 'WooVouCheck', array(
                    'ajaxurl' => admin_url('admin-ajax.php', ( is_ssl() ? 'https' : 'http')),
                    'check_code_error' => __('Please enter voucher code.', 'wcd'),
                    'code_invalid' => __('Voucher code doest not exist.', 'wcd'),
                    'code_used_success' => __('Thank you for your business, voucher code submitted successfully.', 'wcd')
                ));
                wp_register_script('woo-vou-public-script', WOO_VOU_URL . 'includes/js/woo-vou-public.js', array(), WOO_VOU_PLUGIN_VERSION);
                wp_enqueue_script('woo-vou-public-script');
                return $back . do_shortcode('[woo_vou_check_code]');
            }
        }

        ob_start();
        wc_get_template('deal.php', array(), '', WCD_DIR . 'templates/');

        return ob_get_clean();
    }

    add_filter('woocommerce_product_tabs', 'wcd_product_tabs');

    function wcd_product_tabs($tabs = array()) {
        global $product;
        $deal_markers = get_post_meta($product->id, 'deal_markers', true);
        if (!empty($deal_markers)) {
            $tabs['map'] = array(
                'title' => __('Maps and Directions', 'wcd'),
                'priority' => 25,
                'callback' => 'wcd_product_map_tab'
            );
        }
        return $tabs;
    }

    function wcd_product_map_tab() {
        global $product;
        $deal_markers = get_post_meta($product->id, 'deal_markers', true);
        wc_get_template('map_tab.php', array('deal_markers' => $deal_markers), '', WCD_DIR . 'templates/');
    }

}